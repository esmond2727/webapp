const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const sassMiddleware = require('node-sass-middleware');
const favicon = require('serve-favicon');

const indexRouter = require('./routes/index');
const setupRouter = require('./routes/setup');
const userRouter = require('./routes/users');
const surveyRouter = require('./routes/survey');
const courseRouter = require('./routes/course');
const notificationRouter = require('./routes/notification');

const qbRouter = require('./routes/questions');
const fbRouter = require('./routes/feedbacks');

const codeEditorRouter = require('./routes/codeeditor');
const quizAttemptRouter = require('./routes/quizattempt');
const reportRouter = require('./routes/reports');
const healthRouter = require('./routes/health');
const analyticsRouter = require('./routes/analytics');

const redis = require('./lib/redis');
const session = require('express-session');
const redisStore = require('connect-redis')(session);
const appUtil = require('./lib/util');
const log = require('./lib/logger');
const appConf = require('./config');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
log.info(`[INIT] Initialized Redis Session Cache with a timeout expiry of ${redis.config.ttl} seconds`);
app.set('trust proxy', appConf.miscConfigs.trustProxy);
log.info(`[INIT] Trust Proxy: ${appConf.miscConfigs.trustProxy}`);
app.use(favicon(path.join(__dirname, 'public/favicon/favicon.ico')));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(sassMiddleware({
    src: path.join(__dirname, 'public'),
    dest: path.join(__dirname, 'public'),
    indentedSyntax: true, // true = .sass and false = .scss
    sourceMap: true
}));
app.use(express.static(path.join(__dirname, 'public')));

// Init, setup permissions
log.info("Updating Permissions on Redis if exists"); // noinspection JSIgnoredPromiseFromCall
appUtil.forceRefreshPermission();

app.use('/health', healthRouter);

app.use(session({
    secret: redis.config.secret,
    name: '_redisSessionData',
    resave: false,
    saveUninitialized: true,
    cookie: {secure: false,expires: 480000}, // set to 480000 = 8mins then timeout.

    store: new redisStore({
        host: redis.config.host,
        port: redis.config.port,
        password: redis.config.password,
        client: redis.client,
        ttl: redis.config.ttl
    })
}));
app.use('/', indexRouter);
app.use('/setup', setupRouter);
app.use('/users', userRouter);
app.use('/survey', surveyRouter);
app.use('/course', courseRouter);
app.use('/notification', notificationRouter);
app.use('/questions', qbRouter);
app.use('/feedbacks', fbRouter);
app.use('/editor', codeEditorRouter);
app.use('/attempt', quizAttemptRouter);
app.use('/reports', reportRouter);
app.use('/analytics', analyticsRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
    next(createError(404));
});

// error handler
app.use((err, req, res) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

app.set('trust proxy', true)
app.get('/session', function (req, res, next) {
 
    if (req.session.views) {
 
        // Increment the number of views.
        req.session.views++
 
        // Session will expires after 1 min
        // of in activity
        res.write(
            '<p> Session expires after 1 min of in activity: '+ (req.session.cookie.expires) + '</p>')
                    res.end()
                } else {
                    req.session.views = 1
                    res.end(' New session is started')
                }
            })

//cron


require("./utils/cronJob/backend_activity/cron_update_logout_time");

require("./utils/cronJob/email_notification/cron_backup_database");
require("./utils/cronJob/email_notification/cron_unusual_activity");

require("./utils/cronJob/security_maintainability/cron_security");
require("./utils/cronJob/security_maintainability/data_deleter");

module.exports = app;
