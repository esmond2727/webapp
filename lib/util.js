const util = {};
const redis = require('./redis');
const db = require('./db');
const storage = require('./storage');
const fs = require('fs');
const formidable = require('formidable');
const nodeUtil = require('util');
const log = require('../lib/logger');
const moment = require('moment')
util.initialPerm = false;
const getmac = require('getmac').default
const getIp = require('ip');
const ipfetch = require('ip-fetch');
const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
        // user: process.env.EMAIL,
        // pass: process.env.EMAIL_PASSWORD,
        user: 'testappportal9@gmail.com',
        pass: 'cheese28!!!',

    },
});
const { emailinfo } = require("../utils/cronJob/email_notification/cron_unusual_activity_instant");




util.shuffleArray = (array) => {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}

util.generateRandomChar = (length) => {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

util.refreshPermissions = async (userrole) => {
    log.info(`Refreshing ${userrole} permissions from database`);
    let [res] = await db.query("SELECT p.key FROM permissions p WHERE p.reqpower <= (SELECT r.power FROM roles r WHERE r.key = ?)", [userrole]);
    let perms = [];
    res.forEach((i) => {
        perms.push(i.key);
    });
    await redis.set(`roleperm-${userrole}`, JSON.stringify(perms), 'EX', 604800); // Expires every week
    return JSON.stringify(perms);
}

util.forceRefreshPermission = async () => {
    if (!util.initialPerm) {
        try {
            let [roleList] = await db.query("SELECT `key` FROM roles");
            roleList.forEach((role) => {
                // noinspection JSIgnoredPromiseFromCall
                util.refreshPermissions(role.key);
            });
            util.initialPerm = true;
            log.info("Refreshed Permissions");
        } catch (err) {
            log.error("DB Not found. Run /setup first");
        }
    }
}

util.getPermissionList = async (userrole) => {
    log.info(`Obtaining permission list for ${userrole}`);
    let perm = await redis.get(`roleperm-${userrole}`);
    if (perm == null) return await util.refreshPermissions(userrole);
    else return perm;
}

util.hasPermission = async (userrole, permission) => {
    log.info(`Checking for permission ${permission} for ${userrole}`);
    let perm = await util.getPermissionList(userrole);
    return perm.includes(permission);
}

util.capitalize = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

util.getCommitRev = () => {
    let env = util.getEnv();
    if (env === "development") return ` (${require('child_process').execSync('git rev-parse --short HEAD').toString().trim()})`;
    else {
        let path = 'COMMITSHA';
        if (fs.existsSync(path)) return ` (${fs.readFileSync(path, 'utf8').trimEnd()})`;
        else if (process.env.SOURCE_VERSION_SHORT) { log.info(`Found Commit SHA in ENV. Using it`); return ` (${process.env.SOURCE_VERSION_SHORT})`; }
        else {
            // Cannot find the file
            log.warn("Production and failed to find COMMITSHA in app root folder. Omitting Commit Rev");
            return "";
        }
    }
}

util.getEnv = () => {
    return process.env.NODE_ENV || "development";
}

util.getVersion = () => {
    return require('../package.json').version;
}

util.formidablePromise = (req, opts) => {
    return new Promise(function (resolve, reject) {
        let form = new formidable.IncomingForm(opts)
        form.parse(req, function (err, fields, files) {
            if (err) return reject(err)
            resolve({ fields: fields, files: files })
        })
    })
}

util.serialize = (obj) => {
    let str = [];
    for (let p in obj)
        if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
    return str.join("&");
}

util.generateLockedLines = async (template) => {
    if (!Array.isArray(template)) {
        template = template.split('\n');
    }
    let start = [];
    let end  = [];

    template.forEach((line, i) => {
        if (line.includes("%%*")) {
            start.push(i);
        } else if (line.includes("*%%")) {
            end.push(i);
        } else if (line.includes("%%")) {
            start.push(i);
            end.push(i);
        }
    });

    start.sort();
    end.sort();

    let csvLines = "";
    let j = 0;

    start.forEach((num, i) => {
        csvLines += `${num}-`;
        if (end.length > j) {
            csvLines += `${end[j]},`;
            j++;
        } else {
            csvLines += `,`;
        }
    });

    return csvLines.replace(/,\s*$/, "");
}

util.checkLockedLines = async (qnid, qninfo) => {
    for (let tmp of qninfo.code.templates) {
        if (!tmp.locked) {
            log.warn(`No template locks found for language ${tmp.type}. Generating locked lines...`);
            let lines = await util.generateLockedLines(tmp.template);
            log.debug(`Generated Locks: ${lines}`);
            tmp.locked = lines;
            await db.query(`UPDATE question_code_template SET lockedlines=? WHERE questionid=? AND type=?`, [lines, qnid, tmp.type]);
        }
    }
    return qninfo;
}

//Submit Survey or Feedback
util.submitFeedback = async (formUserid, formQuizid, formRating, formFeedback) => {
    log.info('Attempting to submit survey feedback');
    let userid = formUserid;
    let quizid = formQuizid;
    let rating = formRating;
    let feedback = formFeedback;
    try {
        await db.query("INSERT INTO feedbacks (userid, quizid, rating, feedback) VALUES ?", [[[userid, quizid, rating, feedback]]]);
        log.info('Survey feedback successfully submitted!');
        return;
    } catch (err) {
        throw err;
    }
}

//Submit login info
util.submitLoginInfo = async (formUsername, formSalt,formHashpw,sessionid) => {
    log.info('Attempting to submit login');
    let username = formUsername;
    let salt = formSalt;
    let password = formHashpw;
    let ip = getIp.address();
    let mac = getmac();
    let sess = sessionid;
    let startdate = moment(new Date().now).format("YYYY-MM-DD HH:mm:ss");
    let info;
    let country;
    let countrycode;
    let region;
    let regionname;
    let city;
    let zip;
    let latitude;
    let longtitude;
    let timezone;
    let isp;
    let organisation;
    let autonomoussystem;


    let getiplocation = async () =>{
        info = await ipfetch.getLocationNpm("1.1.1.1"); // example => info = await ipfetch.getLocationNpm('1.1.1.1');
      //  console.log(info);
        if (info === undefined )
    {
        country = null;
        countrycode = null;
        region = null;
        regionname = null;
        city = null;
        zip = null;
        latitude = null;
        longtitude = null;
        timezone = null;
        isp = null;
        organisation = null;
        autonomoussystem = null;
      
    }else 
        country = info.country;
        countrycode = info.countryCode;
        region = info.region;
        regionname = info.regionName;
        city = info.city;
        zip = info.zip;
        latitude = info.lat;
        longtitude = info.lon;
        timezone = info.timezone;
        isp = info.isp;
        organisation = info.org;
        autonomoussystem = info.as;


   
        if  (info.country !== 'Singapore'){
    

            // transporter.sendMail(
                
            emailinfo(country, username,ip,startdate),
            function(error, info){
                if (error) {
                  console.log(error);
                } else {
                  console.log('Email sent: ' + info.response);
                }
              }
         //     );
        }
    try {
        await db.query("INSERT INTO logs (username,salt, password,ip,mac,sessionid,logintimestamp, country,countrycode ,region ,regionname ,city ,zip ,latitude ,longtitude ,timezone ,isp ,organisation ,autonomoussystem) VALUES ?",
         [[[username,salt, password,ip,mac,sess,startdate, country,countrycode ,region ,regionname,city ,zip ,latitude ,longtitude ,timezone ,isp , organisation ,autonomoussystem ]]]);
        log.info('Logs successfully submitted!');
        return;
    } catch (err) {
        throw err;
    }
    }
    getiplocation();
    //Then you will get a json object as response. If you need each element as indivual values , then you can do
    // if (info === undefined)
    // {
    //     data = null;
    // }else 
    //     data = "info.country";
        
    // console.log(data)
   
   
}

util.submitLogoutInfo = async (sessionid) => {
    log.info('Attempting to submit login');
    let sessid = sessionid;
    let enddate = moment(new Date().now).format("YYYY-MM-DD HH:mm:ss");
    try {
        await db.query(`update logs set logouttimestamp = "${enddate}" where sessionid = "${sessid}"`);
        log.info('Logs successfully submitted!');
        return;
    } catch (err) {
        throw err;
    }
}


util.readFile = nodeUtil.promisify(fs.readFile);

module.exports = util;