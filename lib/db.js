const mysql = require('mysql2/promise');
const {dbConfig} = require('../config');

const pool = mysql.createPool({
    connectionLimit: 10,
    host: dbConfig.host,
    user: dbConfig.user,
    port: dbConfig.port,
    password: dbConfig.password,
    database: dbConfig.database
});

module.exports = pool;