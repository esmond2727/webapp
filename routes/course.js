const express = require('express');
const router = express.Router();
const auth = require('../lib/auth');
const pwgenerator = require('generate-password');
const {miscConfigs} = require('../config');
const log = require('../lib/logger');

const cache = require('../lib/redis');
const db = require('../lib/db');
const moment = require('moment');
const humanizeDuration = require('humanize-duration');
const csv = require('fast-csv');
const appUtil = require('../lib/util');

const renderConf = {appName: 'Course Management - AASP', route: 'Course'};

router.use('*', async (req, res, next) => { if (await auth.initHeader(req, res, renderConf)) next(); });

// Add new course page
router.get('/add', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'addcourse');
    res.render('courses/addcourse', {...renderConf, title: 'Create Course'});
});

const courseInfoSQL = 'SELECT * FROM courses WHERE id=?'; // Common and used often

// Create a new course
router.post('/add', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'addcourse');
    let enddate = (req.body.enddate) ? req.body.enddate : null;
    let sdchk = moment(new Date(req.body.startdate));
    let startDate = sdchk.format('YYYY-MM-DD HH:mm:ss');
    if (enddate) {
        let edchk = moment(new Date(enddate));
        enddate = edchk.format('YYYY-MM-DD HH:mm:ss');
        if (moment.duration(edchk.diff(sdchk)).asSeconds()<=0) {
            res.render('courses/addcourse', {...renderConf, title: 'Add Course', errmsg: 'End Date cannot be before Start Date'});
            return;
        }
    }
    if (enddate-startDate<=0) {
        res.render('courses/addcourse', {...renderConf, title: 'Add Course', errmsg: 'End Date cannot be before Start Date'});
        return;
    }
    let dbConn = await db.getConnection();

    try {
        await dbConn.beginTransaction();
        let insertCourseSQL = 'INSERT INTO courses(code, name, timestart, timeend, creator) VALUE ?';
        let [data] = await dbConn.query(insertCourseSQL, [[[req.body.code, req.body.name, startDate, enddate, renderConf.loginId]]]);
        await dbConn.query('INSERT INTO course_users (courseid, userid, instructor) VALUE ?', [[[data.insertId, renderConf.loginId, 1]]]);
        await dbConn.commit();
        res.redirect(`/course/list?id=${data.insertId}&action=a`);
    } catch (err) {
        log.error(err);
        await dbConn.rollback();
        res.render('courses/addcourse', {...renderConf, title: 'Add Course', errmsg: 'An error occurred adding this course'});
    } finally {
        await dbConn.release();
    }

});

// List all courses
router.get('/list', async (req, res) => {
    let success = '', failure = '';
    if (req.query.id) {
        let cs = (await db.query(courseInfoSQL, [req.query.id]))[0][0];
        switch (req.query.action) {
            case 'a': success = `Successfully added [${cs.code}] (${cs.name})`; break;
            case 's': success = `Successfully edited [${cs.code}] (${cs.name})`; break;
            case 'sa': success = `Successfully updated user access list to ${cs.code} ${cs.name}`; break;
            case 'sd': success = `Successfully deleted course [${cs.code}] (${cs.name})`; break;
            case 'f': failure = `Failed to edit [${cs.code}] (${cs.name})`; break;
            case 'fa': failure = `Failed to update user list for ${cs.code} ${cs.name}`; break;
            case 'fd': failure = `Failed to delete course`; break;
            case 'fda': failure = `Failed to edit course. End date cannot be before start date`; break;
            case 'np': failure = 'You do not have permission to modify that course'; break;
            case 'na': failure = 'You do not have permission to access that course'; break;
        }
    }
    let isadminLink = (req.query.admin && req.query.admin === 'true');
    let isadmin = (isadminLink) ? "/course/coursesadm" : "/course/courses";
    let newrc = {...renderConf};
    if (isadminLink) newrc.route = 'Admin';

    res.render('courses/listcourse', {...newrc, title: 'List Course', sucmsg: success, errmsg: failure, dataLink: isadmin, adminMode: isadminLink});
});

// Get list of all course data available to the user
router.get('/courses', async (req, res) => {
    let [mangedCourses] = await db.query('SELECT c.* FROM courses c WHERE c.id IN (SELECT cu.courseid FROM course_users cu WHERE userid = ? AND instructor = 1) AND deleted=0', [renderConf.loginId]);
    let [studentCourses] = await db.query('SELECT c.* FROM courses c WHERE c.id IN (SELECT cu.courseid FROM course_users cu WHERE userid = ? AND instructor = 0) AND deleted=0', [renderConf.loginId]);

    // Merge them together somehow
    let data = [];
    let curDate = new Date();

    for (let cc of mangedCourses) {
        let isActive = true;
        if (cc.timeend) {
            let eD = new Date(cc.timeend);
            if (eD.getTime() - curDate.getTime() <= 0) isActive = false;
        }
        data.push({id: cc.id, code: cc.code, name: cc.name, ts: cc.timestart, te: cc.timeend, creator: cc.creator, canEdit: true, isActive: isActive});
    }
    for (let sc of studentCourses) {
        let isActive = true;
        if (sc.timeend) {
            let eD = new Date(sc.timeend);
            if (eD.getTime() - curDate.getTime() <= 0) isActive = false;
        }
        data.push({id: sc.id, code: sc.code, name: sc.name, ts: sc.timestart, te: sc.timeend, creator: sc.creator, canEdit: false, isActive: isActive});
    }
    res.json({data: data});
});

router.get('/coursesadm', async (req, res) => {
    let [allCourse] = await db.query('SELECT c.* FROM courses c WHERE deleted=0');

    // Merge them together somehow
    let data = [];
    let curDate = new Date();

    for (let cc of allCourse) {
        let isActive = true;
        if (cc.timeend) {
            let eD = new Date(cc.timeend);
            if (eD.getTime() - curDate.getTime() <= 0) isActive = false;
        }
        data.push({id: cc.id, code: cc.code, name: cc.name, ts: cc.timestart, te: cc.timeend, creator: cc.creator, canEdit: true, isActive: isActive});
    }
    res.json({data: data});
});

async function canModifyThisCourse(res, courseid) { return await canModifyThisCourseRed(res, courseid, true); }

async function canModifyThisCourseRed(res, courseid, redirect) {
    if (await auth.hasPermissionBool(renderConf, "manageallquiz")) return true;
    let [chk] = await db.query('SELECT * FROM course_users WHERE userid=? AND courseid=? AND instructor=1', [renderConf.loginId, courseid]);
    let course = (await db.query('SELECT * FROM courses WHERE id=? AND deleted=0', [courseid]))[0][0];
    if (!chk || chk.length <= 0 || !course) { if (redirect) res.redirect(`/course/list?action=np&id=${courseid}`); return false; } // If no permission to touch this course
    return true;
}

async function canAccessCourse(res, courseid) {
    if (await auth.hasPermissionBool(renderConf, "manageallquiz")) return true;
    let [chk] = await db.query('SELECT * FROM course_users WHERE userid=? AND courseid=?', [renderConf.loginId, courseid]);
    let course = (await db.query('SELECT * FROM courses WHERE id=? AND deleted=0', [courseid]))[0][0];
    if (!chk || chk.length <= 0 || !course) { res.redirect(`/course/list?action=na&id=${courseid}`); return false; } // If no permission to touch this course
    return true;
}

// View edit course page
router.get('/edit/:courseid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'editcourse');
    if (!(await canModifyThisCourse(res, req.params.courseid))) return;
    let course = (await db.query(courseInfoSQL, [req.params.courseid]))[0][0];
    course.st = moment(new Date(course.timestart)).format('YYYY-MM-DD');
    course.et = (course.timeend) ? moment(new Date(course.timeend)).format('YYYY-MM-DD') : '';
    course.permanent = (!course.timeend)
    res.render('courses/editcourse', {...renderConf, data: course, title: `Edit ${course.name}`});
});

// Delete course
router.get('/delete/:courseid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'editcourse');
    if (!(await canModifyThisCourse(res, req.params.courseid))) return;
    try {
        let deleteCourseSQL = 'UPDATE courses SET deleted=1 WHERE id=?';
        await db.query(deleteCourseSQL, [req.params.courseid]);
        res.redirect(`/course/list?id=${req.params.courseid}&action=sd`);
    } catch (err) {
        log.error(err);
        res.redirect(`/course/list?id=${req.params.courseid}&action=fd`);
    }
});

// Update course
router.post('/edit/:courseid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'editcourse');
    let enddate = (req.body.enddate) ? req.body.enddate : null;
    let sdchk = moment(new Date(req.body.startdate));
    let startDate = sdchk.format('YYYY-MM-DD HH:mm:ss');
    if (enddate) {
        let edchk = moment(new Date(enddate));
        enddate = edchk.format('YYYY-MM-DD HH:mm:ss');
        if (moment.duration(edchk.diff(sdchk)).asSeconds()<=0) {
            res.redirect(`/course/list?id=${req.params.courseid}&action=fda`);
            return;
        }
    }
    let query = 'UPDATE courses SET code=?, name=?, timestart=?, timeend=? WHERE id=?'; // The where clause is below
    let opt = [req.body.code, req.body.name, startDate]
    opt.push((enddate) ? enddate : null);
    opt.push(req.params.courseid);
    try {
        await db.query(query, opt);
        res.redirect(`/course/list?id=${req.params.courseid}&action=s`);
    } catch (err) {
        log.error(err);
        res.redirect(`/course/list?id=${req.params.courseid}&action=f`);
    }
});

// View assign users to course page
router.get('/assign/:courseid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'assigncourse');
    if (!(await canModifyThisCourse(res, req.params.courseid))) return;
    let data = (await db.query(courseInfoSQL, [req.params.courseid]))[0][0];
    //let [user] = await db.query('SELECT id, username, fullname, email, role, department FROM users');
    let [studentUsers] = await db.query('SELECT id, username, fullname, email, role, department FROM users WHERE role = 1');
    let [powerUsers] = await db.query('SELECT id, username, fullname, email, role, department FROM users WHERE role > 1');
    let userListSQL = 'SELECT userid, instructor FROM course_users WHERE courseid = ?';
    let [userlist] = await db.query(userListSQL, [req.params.courseid]);
    let student = [], instructor = [];
    for (let u of userlist) {
        if (u.instructor === 1){ 
            instructor.push(u.userid);
        }
        else{ student.push(u.userid);}
    }
    res.render('courses/assigncourse', {...renderConf, data: data, title: `Assign Users to ${data.code}`, studentUsers: studentUsers, powerUsers: powerUsers, studentList: student, instructorList: instructor});
});

// View import useres into course page
router.get('/importusers/:courseid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'assigncourse');
    if (!(await canModifyThisCourse(res, req.params.courseid))) return;
    let data = (await db.query(courseInfoSQL, [req.params.courseid]))[0][0];
    res.render('courses/bulkassignuser', {...renderConf, title: `Bulk Assign Users to ${data.code}`, data: data});
});

// Import users into course
router.post('/importusers/:courseid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'assigncourse');
    if (!(await canModifyThisCourse(res, req.params.courseid))) return;
    let data = (await db.query(courseInfoSQL, [req.params.courseid]))[0][0];
    try {
        let form = await appUtil.formidablePromise(req);
        let csvFile = form.files.assignusers;
        log.info(`Received '${csvFile.name}' with type ${csvFile.type} of size ${csvFile.size} at ${csvFile.path}`);

        // Validate that we have a CSV file
        let success = true;
        let error = '';
        if (csvFile.size <= 0) {
            success = false;
            error = 'Invalid File';
        } else if (csvFile.type !== 'text/csv') {
            success = false;
            error = 'File uploaded is not a CSV file';
        }

        if (!success) {
            res.render('courses/bulkassignuser', {...renderConf, title: `Bulk Assign Users to ${data.code}`, data: data, errmsg: error});
            return;
        }

        // Process CSV file (try to, final parsing)
        let parsedCSV = csv.parseFile(csvFile.path, {headers: ["username", "status"], renameHeaders: true, ignoreEmpty: true, discardUnmappedColumns: true});
        parsedCSV.on('error', error => {
            log.info(error);
            res.render('courses/bulkassignuser', {...renderConf, title: `Bulk Assign Users to ${data.code}`, data: data, errmsg: "An error occurred parsing this CSV file"});
        });
        let streamData = [];
        let errData = [], userData = [];
        parsedCSV.on('data', row => streamData.push(row));
        parsedCSV.on('end', async rowCount => {
            // Process End, further processing
            log.info(`Parsed ${rowCount} rows`);
            for (let row of streamData) {
                // Check user valid or not
                let user = (await db.query('SELECT * FROM users WHERE username=? AND disabled=0', [row.username]))[0][0];
                if (!user) {
                    log.info(`Error processing row for ${row.username} (invalid user)`);
                    row.errorAt = "Invalid Users";
                    errData.push(row);
                    continue;
                }
                if (!row.status || isNaN(row.status) || parseInt(row.status) > 1 || parseInt(row.status) < 0) row.status = 0;
                else row.status = parseInt(row.status);
                row.fullname = user.fullname;
                row.userid = user.id;
                row.course = req.params.courseid;
                userData.push(row);
            }
            log.info(`Success ${userData.length} records, Failed ${errData.length} rows`)
            if (userData.length <= 0) res.render('courses/bulkassignuser', {...renderConf, title: `Bulk Assign Users to ${data.code}`, data: data, errmsg: "No user accounts found in file that matches users in system"});
            else {
                // Place data on redis with a TTL to expire and generate a random id for me
                let rediskey = `assignuser:${new Date().getTime()}:${auth.getSalt()}`;
                await cache.set(rediskey, JSON.stringify(userData), 'EX', 10800); // Set redis for 3 hours
                res.render('courses/bulkassignusercheck', {...renderConf, title: `Confirm Assign Users to ${data.code}`, data: data, errData: errData, dataCache: rediskey, userlen: userData.length});
            }
        });
    } catch (err) {
        log.error(err);
        res.render('courses/bulkassignuser', {...renderConf, title: `Bulk Assign Users to ${data.code}`, data: data, errmsg: "An error occurred uploading file"});
    }
});

async function bulkAssignUsers(redisKey, cachekey) {
    await cache.set(cachekey, JSON.stringify({status: "Getting data from cache", finished: false}), 'EX', 10800);
    let data = await cache.get(redisKey);
    data = JSON.parse(data);
    await cache.set(cachekey, JSON.stringify({status: `Preparing to add ${data.length} users`, finshed: false}), 'EX', 10800);
    let cnt = 0;
    let errRow = [];
    let dbConn = await db.getConnection();
    try {
        await dbConn.beginTransaction();
        for (let d of data) {
            cnt++;
            await cache.set(cachekey, JSON.stringify({status: `Assigning ${cnt}/${data.length} users to course`, finished: false}), 'EX', 10800);
            await dbConn.query("DELETE FROM course_users WHERE courseid=? AND userid=?", [d.course, d.userid]);
            if (d.status === -1)
                await dbConn.query('INSERT INTO course_users(courseid, userid, instructor) VALUES ?', [[[d.course, d.userid, d.status]]]); // Add instructors
        }
        await dbConn.commit();
        await cache.set(cachekey, JSON.stringify({status: `Finished assigning ${cnt} users! Redirecting you back shortly...`, finished: true, errors: errRow, suc: true}), 'EX', 10800);
    } catch (err) {
        log.error(err);
        await dbConn.rollback();
        await cache.set(cachekey, JSON.stringify({status: `Failed to assign users (DB Error). Redirecting you back shortly...`, finished: true, errors: errRow, suc: false}), 'EX', 10800);
    } finally {
        await dbConn.release();
    }
}

// Confirm that we are importing users into course
router.post('/importusers/:courseid/confirm', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'assigncourse');
    let redisKey = req.body.cachekey;
    let newrediskey = `processAssignU-${auth.getSalt()}`;
    await cache.set(newrediskey, JSON.stringify({status: "Processing", finished: false}), 'EX', 10800);
    bulkAssignUsers(redisKey, newrediskey).then(() => log.info("Bulk add completed"));
    res.render('courses/bulkassignprogress', {...renderConf, title: 'Processing User Assignment...', processKey: newrediskey, cid: req.params.courseid});
});

// Get user import to course status
router.get('/importusers/:key/status', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'assigncourse');
    let info = await cache.get(req.params.key);
    res.json(JSON.parse(info));
});

// Assign users to course
router.post('/assign/:courseid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'assigncourse');
    let studentList = [];
    let instructorList = [];
    if (req.body.studentlist) {
        if (Array.isArray(req.body.studentlist)) studentList=req.body.studentlist;
        else studentList.push(req.body.studentlist);
    }
    if (req.body.instructorlist) {
        if (Array.isArray(req.body.instructorlist)) instructorList=req.body.instructorlist;
        else instructorList.push(req.body.instructorlist);
    }

    let instructorOpts = [];
    let studentOpts = [];
    for (let i of instructorList) instructorOpts.push([req.params.courseid, i, 1]);
    for (let i of studentList) studentOpts.push([req.params.courseid, i, 0]);
    let dbConn = await db.getConnection(); // Assigning users in transaction
    try {
        await dbConn.beginTransaction();
        let deleteUserSQL = 'DELETE FROM course_users WHERE courseid=?';
        await dbConn.query(deleteUserSQL, [req.params.courseid]); // Remove all users
        await dbConn.query('INSERT INTO course_users(courseid, userid, instructor) VALUES ?', [instructorOpts]); // Add instructors
        if (studentList.length > 0) await dbConn.query('INSERT INTO course_users(courseid, userid, instructor) VALUES ?', [studentOpts]); // Add students
        await dbConn.commit();
        res.redirect(`/course/list?id=${req.params.courseid}&action=sa`);
    } catch (e) {
        log.info(e);
        log.info("Rollback previous changes");
        await dbConn.rollback()
        res.redirect(`/course/list?id=${req.params.courseid}&action=fa`);
    } finally {
        await dbConn.release();
    }
});

// Get info and list of all quizzes in course
router.get('/info/:courseid', async (req, res) => {
    if (!(await canAccessCourse(res, req.params.courseid))) return;
    let courseinfo = (await db.query(courseInfoSQL, [req.params.courseid]))[0][0];
    let option = {...renderConf, title: `[${courseinfo.code}] ${courseinfo.name}`, info:courseinfo, appName: 'AASP'}
    if (req.query.act) {
        switch (req.query.act) {
            case 'ne': option.errmsg = 'Quiz not found'; break;
            case 'nea': option.errmsg = 'Quiz is not available at this time'; break;
        }
    }
    if (req.query.edit && req.query.edit === '1') {
        if (!(await canModifyThisCourseRed(res, req.params.courseid, false))) {res.redirect(`/course/info/${req.params.courseid}`); return; } // Kick out of editing mode
        option.title = `Edit ${option.title}`
        if (req.query.id) {
            let quiznameSQL = 'SELECT name FROM quiz WHERE id=?';
            let quizname = (await db.query(quiznameSQL, [req.query.id]))[0][0]; // We want to include deleted quizzes here as we want the name
            switch (req.query.act) {
                case 'suc': option.sucmsg = `Successfully added quiz "${quizname.name}" into the course`; break;
                case 'su': option.sucmsg = `Successfully updated "${quizname.name}"`; break;
                case 'suha': option.sucmsg = `Partially updated "${quizname.name}". As there are existing quiz attempts, questions are not updated`; break;
                case 'fu': option.errmsg = `Failed to update "${quizname.name}"`; break;
                case 'fua': option.errmsg = `Failed to update "${quizname.name}". Start time cannot be after End Time!`; break;
                case 'nogo': option.errmsg = `Quiz does not exist`; break;
                case 'timeout': option.errmsg = `Timeout occurred editing quiz ${quizname.name}`; break;
                case 'del': option.sucmsg = `Successfully deleted quiz "${quizname.name}"`; break;
                case 'delf': option.errmsg = `Failed to delete quiz "${quizname.name}"`; break;
            }
        }
        res.render('courses/viewcourseedit', option);
    } else {
        // Check if user can edit and redirect to support edit
        let canEditC = await canModifyThisCourseRed(res, req.params.courseid, false);
        if (canEditC && !req.query.edit) {
            res.redirect(`/course/info/${req.params.courseid}?edit=1&${appUtil.serialize(req.query)}`);
            return;
        }
        res.render('courses/viewcourse', option);
    }
});

function processQuizRecord(quiz, curDate) {
    quiz.isActive = true;
    quiz.isUnstarted = false;
    if (quiz.timeend) {
        // Check if quiz has ended
        let eD = new Date(quiz.timeend);
        if (eD.getTime() - curDate.getTime() <= 0) quiz.isActive = false;
    }
    let sD = new Date(quiz.timestart);
    if (curDate.getTime() - sD.getTime() <= 0) { quiz.isActive = false; quiz.isUnstarted = true; }

    if (quiz.attempts < 0) quiz.attempts = 'Unlimited';
    if (quiz.timelimit <= 0) quiz.timelimit = 'No Time Limit';
    else quiz.timelimit = humanizeDuration(quiz.timelimit * 1000); // Convert seconds -> milliseconds
    if (quiz.totalscore) quiz.totalscore = parseInt(quiz.totalscore + "");
    return quiz;
}

// Get data of all quizzes in a course
router.get('/info/:courseid/data', async (req, res) => {
    if (!(await canAccessCourse(res, req.params.courseid))) return;
    let quizInfoSQL = 'SELECT * FROM quiz WHERE courseid=? AND deleted=0';
    let [quizlist] = await db.query(quizInfoSQL, [req.params.courseid]);
    let canEdit = await canModifyThisCourseRed(res, req.params.courseid, false);

    let data = [];
    let curDate = new Date();

    for (let quiz of quizlist) {
        quiz = processQuizRecord(quiz, curDate);
        if (quiz.isUnstarted && !req.query.edit) continue; // Do not add any unstarted quizzes unless editing mode is on
        let [attemptCount] = await db.query("SELECT COUNT(id) as cid FROM quiz_attempt WHERE quizid=? AND studentid=?", [quiz.id, renderConf.loginId]);
        if (attemptCount && attemptCount.length > 0) attemptCount = attemptCount[0].cid;
        else attemptCount = 0;
        data.push({id: quiz.id, courseid: quiz.courseid, name: quiz.name, ts: quiz.timestart, te: quiz.timeend, tl: quiz.timelimit,
            attempts: quiz.attempts, canEdit: canEdit, isUnstarted: quiz.isUnstarted, isActive: quiz.isActive, desc: quiz.description, attempted: attemptCount});
    }
    res.json({data: data});
});

// Add a new quiz to the course page
router.get('/info/:courseid/add', async (req, res) => {
    if (!(await canModifyThisCourseRed(res, req.params.courseid, false))) {res.redirect(`/course/info/${req.params.courseid}`); return; } // Kick out of editing mode
    let [qnBanks] = await db.query('SELECT * FROM questionsbank WHERE id IN (SELECT bankid FROM questionbank_access WHERE userid = ?) AND deleted=0', [renderConf.loginId]);
    let errmsg;
    if (req.query.err) {
        switch (req.query.err) {
            case '1': errmsg = 'An error occurred adding the quiz'; break;
            case '2': errmsg = 'An error occurred adding the quiz. (Start time cannot be after end time)'; break;
        }
    }
    res.render('quiz/addquiz', {...renderConf, title: 'Add New Quiz', editorFPRaw: `course/${req.params.courseid}/`, courseid: req.params.courseid, banks: qnBanks, errmsg: errmsg});
});

function updateBasedOnQuizType(quizdata, type) {
    switch (type) {
        case 0: // Quiz. NOP
            break;
        case 1: // Assignment. No durations
            quizdata.timelimit = null;
            break;
        case 2: // Practice. No durations & attempts
            quizdata.timelimit = null;
            quizdata.attempts = null;
            break;
    }
}

// Add a new quiz to the course
router.post('/info/:courseid/add', async (req, res) => {
    if (!(await canModifyThisCourseRed(res, req.params.courseid, false))) {res.redirect(`/course/info/${req.params.courseid}`); return; } // Kick out of editing mode
    let redisKey = `modifyquiz-${renderConf.loginId}-${req.params.courseid}--1`;
    let qt = parseInt(req.body.quizType);
    let quizdata = {name: req.body.name, description: (req.body.desc) ? req.body.desc : null, quiztype: parseInt(req.body.quizType), creator: renderConf.loginId};
    updateBasedOnQuizType(quizdata, qt);
    try {
        await cache.setDefault(redisKey, JSON.stringify(quizdata));
        res.redirect(`/course/info/${req.params.courseid}/-1/overview`);
    } catch (err) {
        log.error(err);
        res.redirect(`/course/info/${req.params.courseid}/add?err=1`);
    }
});

// Get quiz add/edit overview page
router.get('/info/:courseid/:quizid/overview', async (req, res) => {
    if (!(await canModifyThisCourseRed(res, req.params.courseid, false))) {res.redirect(`/course/info/${req.params.courseid}`); return; } // Kick out of editing mode
    let quizid = req.params.quizid;
    let cid = req.params.courseid;
    let redisKey = `modifyquiz-${renderConf.loginId}-${cid}-${quizid}`;

    let quizdata;
    if (quizid !== '-1' && !req.query.edit) {
        // Get from SQL, populate to Redis
        let quiz = (await db.query('SELECT * FROM quiz WHERE id=? AND deleted=0', [quizid]))[0][0];
        if (!quiz) { res.redirect(`/course/info/${req.params.courseid}?edit=1&id=${req.params.quizid}&act=nogo`); }
        if (quiz.attempts === -1) quiz.attempts = null;
        if (quiz.timelimit === -1) quiz.timelimit = null;
        if (parseFloat(quiz.totalscore) === -1.0) quiz.totalscore = null;

        // Get Questions
        let [qnSections] = await db.query("SELECT * FROM quiz_sections WHERE quizid=? AND deleted=0 ORDER BY secorder", quizid);
        quiz.sections = {list:[]};
        for (let qnSec of qnSections) {
            let [questions] = await db.query("SELECT * FROM quiz_questions WHERE sectionid=? AND deleted=0", qnSec.id);
            let secQns = [];
            for (let qRec of questions) {
                let qInfoRec = (await db.query("SELECT q.id,q.name,q.maxscore,q.type,b.name AS bankname FROM question q JOIN questionsbank b on q.questionbankid = b.id WHERE q.id = ? AND q.deleted=0", qRec.questionid))[0][0];
                secQns.push({id: qInfoRec.id, name: qInfoRec.name, score: qInfoRec.maxscore, type: qInfoRec.type, bankname: qInfoRec.bankname, opt: qRec.opt, typeName: getName(qInfoRec.type)});
            }
            if (qnSec.name === "def") quiz.sections.def = {name: qnSec.name, questions: secQns, count: secQns.length, noofqns: qnSec.noofquestions};
            else quiz.sections.list.push({name: qnSec.name, questions: secQns, count: secQns.length, order: qnSec.secorder, noofqns: qnSec.noofquestions});
        }

        await cache.setDefault(redisKey, JSON.stringify(quiz));
    }

    // Get data from key
    quizdata = await cache.get(redisKey);
    quizdata = JSON.parse(quizdata);
    if (!quizdata) { res.redirect(`/course/info/${req.params.courseid}?edit=1&id=${req.params.quizid}&act=timeout`); }

    if (quizdata.attempts === -1) quizdata.attempts = null;
    if (quizdata.timelimit === -1) quizdata.timelimit = null;

    if (!quizdata.showgrade) quizdata.showgrade = 0;
    if (!quizdata.studentreport) quizdata.studentreport = 0;

    quizdata.timestart = moment(new Date(quizdata.timestart)).format('DD MMMM YYYY HH:mm');
    quizdata.timeend = (quizdata.timeend) ? moment(new Date(quizdata.timeend)).format('DD MMMM YYYY HH:mm') : null;
    quizdata.quiztypeName = 'Unknown';
    switch (quizdata.quiztype) {
        case 0: quizdata.quiztypeName = 'Quiz'; break;
        case 1: quizdata.quiztypeName = 'Assignment'; break;
        case 2: quizdata.quiztypeName = 'Practice'; break;
    }
    if (!quizdata.timelimit || quizdata.timelimit <= 0) quizdata.timelimit = 'No Time Limit';
    else quizdata.timelimit = humanizeDuration(quizdata.timelimit * 1000); // Convert seconds -> milliseconds
    quizdata.qncount = 0;
    if (quizdata.sections) {
        if (quizdata.sections.def) quizdata.qncount += quizdata.sections.def.count;
        if (quizdata.sections.list) {
            for (let qs of quizdata.sections.list) {
                quizdata.qncount += qs.count;
            }
        } else quizdata.sections.list = [];
    } else quizdata.sections = {list:[], def:{name:"def", questions:[],count:0, noofqns:-1}};

    if (quizid === '-1' && !req.query.edit) await cache.setDefault(redisKey, JSON.stringify(quizdata));

    let sucmsg, errmsg;

    if (req.query.act) {
        switch (req.query.act) {
            case 'su': sucmsg = `[LEGACY] Successfully edited "${quizdata.name}"`; break;
            case 'suu': sucmsg = `Successfully updated quiz settings for "${quizdata.name}"`; break;
            case 'fu': errmsg = `[LEGACY] Failed to edit "${quizdata.name}"`; break;
            case 'fuasd': errmsg = `Failed to save "${quizdata.name}". Quiz Start date not filled. Fill in with the 'Change Duration' action`; break;
            case 'fuu': errmsg = `Failed to update quiz settings for "${quizdata.name}"`; break;
            case 'fua': errmsg = `Failed to update "${quizdata.name}". Start time cannot be after End Time!`; break;
            case 'fut': errmsg = `Invalid edit action`; break;
        }
    }

    res.render('quiz/quizoverview', {...renderConf, courseid: cid, quizid: quizid, data: quizdata, title: `Quiz Overview - ${quizdata.name}`, sucmsg: sucmsg, errmsg: errmsg});
});

// Get quiz status of who started/unstarted the quiz
 router.get('/info/:courseid/:quizid/quizstatus', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'assigncourse');
    if (!(await canModifyThisCourse(res, req.params.courseid))) return;
     let courseid = req.params.courseid;
     let quizid = req.params.quizid;
     let getStatus = "/course/"+courseid+"/"+quizid+"/getQuizStatus";
     let courseinfo = (await db.query(courseInfoSQL, [req.params.courseid]))[0][0];
     let updatedTime = new Date().toLocaleString().replace(',','');
     let [noOfStudentsInCourse] = await db.query('SELECT COUNT(*) AS count FROM course_users WHERE courseid=? AND instructor=0', [courseid]);
     let [StudentsDoneQuiz] = await db.query('SELECT DISTINCT studentid FROM quiz_attempt WHERE quizid=? AND markeddate IS NOT NULL', [quizid]);
     let noOfStudentsDoneQuiz = StudentsDoneQuiz.length;
     let participationRate = ((noOfStudentsDoneQuiz/noOfStudentsInCourse[0].count)*100).toFixed(2);
     let [unstarted] = await db.query('SELECT COUNT(*) AS count FROM course_users cu LEFT JOIN quiz_attempt qa ON qa.studentid=cu.userid WHERE cu.courseid=? AND cu.instructor=0 AND qa.date IS NULL AND qa.submitteddate IS NULL AND qa.markeddate IS NULL', [quizid]);
     let [started] = await db.query('SELECT COUNT(*) AS count FROM quiz_attempt WHERE quizid = ? AND (date IS NOT NULL AND submitteddate IS NULL)', [quizid]);
     let [completed] = await db.query('SELECT DISTINCT studentid FROM quiz_attempt WHERE quizid = ? AND date < submitteddate < markeddate', [quizid]);
     res.render('courses/quizstatus', {...renderConf, status: getStatus, info: courseinfo, updatedTime: updatedTime, 
        totalStudents: noOfStudentsInCourse[0].count, studentDone: noOfStudentsDoneQuiz, partRate: participationRate,
        unstartedCount: unstarted[0].count, startedCount: started[0].count,completedCount: completed.length});
 });

// Get JSON of Quiz Status
 router.get('/:courseid/:quizid/getQuizStatus', async (req, res) => {
    let courseid = req.params.courseid; 
    let quizid = req.params.quizid;
    let [attempts] = await db.query('SELECT * FROM (SELECT cu.userid, u.fullname, u.email, cu.courseid, cu.instructor FROM course_users cu JOIN users u ON cu.userid = u.id WHERE cu.courseid = ? AND cu.instructor = 0) AS pt1 LEFT JOIN (SELECT * FROM (SELECT * FROM quiz_attempt qa WHERE qa.date = (SELECT MAX(qa2.date) FROM quiz_attempt qa2 WHERE qa2.studentid = qa.studentid)) AS qaz WHERE qaz.quizid=? OR qaz.quizid IS NULL) AS pt2 ON pt1.userid = pt2.studentid', [courseid, quizid]);
    let data = [];
    let attemptd, formatted_attemptd, submittedd, formatted_submittedd, markedd, formatted_markedd;
    let statusDescription;
    for (let attempt of attempts) { 
        // Format the dates
            // Attempt Date
            attemptd = (attempt.date) ? moment(attempt.date) : "No Attempts";
            if(attemptd != "No Attempts")
                formatted_attemptd = attemptd.format('YYYY-MM-DD HH:mm');
            else
                formatted_attemptd = attemptd;

            // Submitted Date
            submittedd = (attempt.submitteddate) ? moment(attempt.submitteddate) : "No Submission";
            if(submittedd != "No Submission")
                formatted_submittedd = submittedd.format('YYYY-MM-DD HH:mm');
            else
                formatted_submittedd = submittedd;

            // Marked Date
            markedd = (attempt.markeddate) ? moment(attempt.markeddate) : "Not Marked";
            if(markedd != "Not Marked")
                formatted_markedd = markedd.format('YYYY-MM-DD HH:mm');
            else
                formatted_markedd = markedd;
        
        // Status classification (0-Generating, 1-Started, 2-Completed and Pending Marking, 3-Marking, 4-Marked, 5- Error)
            switch(attempt.status){
                case 0: 
                    statusDescription = "Generating";
                    break;
                case 1: 
                    statusDescription = "Started";
                    break;
                case 2:
                    statusDescription = "Completed (Pending Marking)";
                    break;
                case 3:
                    statusDescription = "Marking";
                    break;
                case 4:
                    statusDescription = "Marked";
                    break;
                case 5:
                    statusDescription = "Error";
                    break;
                default:
                    statusDescription = "Not Started"
                    break;
            }
        data.push({
            fullname: attempt.fullname, 
            email: attempt.email,
            date: formatted_attemptd, 
            status: statusDescription, 
            score:attempt.score, 
            submitteddate: formatted_submittedd,
            markeddate: formatted_markedd
        }); 
    }
    res.json({data: data});
});

// Update quiz details in a course
router.post('/info/:courseid/:quizid/save', async (req, res) => {
    if (!(await canModifyThisCourseRed(res, req.params.courseid, false))) {res.redirect(`/course/info/${req.params.courseid}`); return; }
    let quizid = req.params.quizid;
    let cid = req.params.courseid;
    let redisKey = `modifyquiz-${renderConf.loginId}-${cid}-${quizid}`;

    // Get data from redis
    let quizdata = await cache.get(redisKey);
    quizdata = JSON.parse(quizdata);

    if (!quizdata.timestart || quizdata.timestart === "Invalid date") {
        res.redirect(`/course/info/${req.params.courseid}/${req.params.quizid}/overview?act=fuasd&edit=1`);
        return;
    }

    let enddate = (quizdata.timeend) ? quizdata.timeend : null;
    let sdchk = moment(quizdata.timestart);
    let startDate = sdchk.format('YYYY-MM-DD HH:mm:ss');
    if (enddate) {
        let edchk = moment(enddate);
        enddate = edchk.format('YYYY-MM-DD HH:mm:ss');
    }

    let dbConn = await db.getConnection();
    let isInsert = false;
    let mainStmt, mainData;

    if (quizid !== "-1") {
        mainStmt = 'UPDATE quiz SET name=?, timestart=?, timeend=?, timelimit=?, attempts=?, creator=?, password=?, passwordgen=?, totalscore=?, showgrade=?, studentreport=?, description=?, preinstructions=?, quiztype=?, otptimelimit=? WHERE id=?';
        mainData = [quizdata.name, startDate, enddate, (quizdata.timelimit) ? quizdata.timelimit : -1,
            (quizdata.attempts) ? quizdata.attempts : -1, quizdata.creator, (quizdata.password) ? quizdata.password : null,
            (quizdata.passwordgen) ? 1 : 0, (quizdata.totalscore) ? quizdata.totalscore : -1,
            (quizdata.showgrade) ? quizdata.showgrade : 0, (quizdata.studentreport) ? quizdata.studentreport : 0,
            (quizdata.description) ? quizdata.description : null, (quizdata.preinstructions) ? quizdata.preinstructions : null, quizdata.quiztype,
            (quizdata.otptimelimit) ? quizdata.otptimelimit : 0, quizid];
    } else {
        isInsert = true;
        mainStmt = 'INSERT INTO quiz (courseid, name, timestart, timeend, timelimit, attempts, creator, password, passwordgen, totalscore, showgrade, studentreport, description, preinstructions, otptimelimit, quiztype) VALUES ?';
        mainData = [[[parseInt(cid), quizdata.name, startDate, enddate, (quizdata.timelimit) ? quizdata.timelimit : -1,
            (quizdata.attempts) ? quizdata.attempts : -1, quizdata.creator, (quizdata.password) ? quizdata.password : null,
            (quizdata.passwordgen) ? 1 : 0, (quizdata.totalscore) ? quizdata.totalscore : -1,
            (quizdata.showgrade) ? quizdata.showgrade : 0, (quizdata.studentreport) ? quizdata.studentreport : 0,
            (quizdata.description) ? quizdata.description : null, (quizdata.preinstructions) ? quizdata.preinstructions : null,
            (quizdata.otptimelimit) ? quizdata.otptimelimit : 0, quizdata.quiztype]]];
    }

    let sectionInsertStmt = "INSERT INTO quiz_sections(quizid, name, noofquestions, secorder) VALUES ?";
    let qnInsertStmt = "INSERT INTO quiz_questions(quizid, questionid, sectionid, opt) VALUES ?";

    try {
        await dbConn.beginTransaction();
        let [data] = await dbConn.query(mainStmt, mainData); // Update main record
        let qid = (isInsert) ? data.insertId : req.params.quizid;

        // Update sections and questions (delete and reinsert)
        let hasAttempt = false;
        if (quizdata.sections) {
            try {
                await dbConn.query("UPDATE quiz_questions SET deleted=1 WHERE quizid=?", qid);
                await dbConn.query("UPDATE quiz_sections SET deleted=1 WHERE quizid=?", qid);

                // Go through section array
                if (quizdata.sections.list) {
                    for (let secData of quizdata.sections.list) {
                        let [secs] = await dbConn.query(sectionInsertStmt, [[[qid, secData.name, secData.noofqns, secData.order]]]);
                        let secqnArr = [];
                        for (let secqn of secData.questions) {
                            secqnArr.push([qid, secqn.id, secs.insertId, secqn.opt])
                        }
                        if (secqnArr.length !== 0) await dbConn.query(qnInsertStmt, [secqnArr]);
                    }
                }

                // Default Section
                if (quizdata.sections.def) {
                    let sec = quizdata.sections.def;
                    let qnCount = sec.questions.length;
                    let [secs] = await dbConn.query(sectionInsertStmt, [[[qid, sec.name, qnCount, -1]]]);
                    let secqnArr = [];
                    for (let secqn of sec.questions) {
                        secqnArr.push([qid, secqn.id, secs.insertId, secqn.opt])
                    }
                    if (secqnArr.length !== 0) await dbConn.query(qnInsertStmt, [secqnArr]);
                }
            } catch (err) {
                if (err.code === "ER_ROW_IS_REFERENCED_2") {
                    hasAttempt = true;
                } else {
                    await dbConn.rollback()
                    log.error(err);
                    if (isInsert) res.redirect(`/course/info/${req.params.courseid}/add?err=1`);
                    else res.redirect(`/course/info/${req.params.courseid}?edit=1&id=${req.params.quizid}&act=fu`);
                }
            }
        }

        await dbConn.commit();
        await cache.set(redisKey, '', 'EX', 2);
        if (isInsert) res.redirect(`/course/info/${req.params.courseid}?edit=1&id=${qid}&act=suc`);
        else if (hasAttempt) res.redirect(`/course/info/${req.params.courseid}?edit=1&id=${qid}&act=suha`);
        else res.redirect(`/course/info/${req.params.courseid}?edit=1&id=${qid}&act=su`);
    } catch (err) {
        await dbConn.rollback()
        log.error(err);
        if (isInsert) res.redirect(`/course/info/${req.params.courseid}/add?err=1`);
        else res.redirect(`/course/info/${req.params.courseid}?edit=1&id=${req.params.quizid}&act=fu`);
    } finally {
        await dbConn.release();
    }

});

// Edit a specific feature of a quiz page
router.get('/info/:courseid/:quizid/edit/:type', async (req, res) => {
    if (!(await canModifyThisCourseRed(res, req.params.courseid, false))) {res.redirect(`/course/info/${req.params.courseid}`); return; } // Kick out of editing mode
    let quizid = req.params.quizid;
    let cid = req.params.courseid;
    let type = req.params.type;
    let redisKey = `modifyquiz-${renderConf.loginId}-${cid}-${quizid}`;

    let quizdata = await cache.get(redisKey);
    quizdata = JSON.parse(quizdata);
    if (quizdata.attempts === -1) quizdata.attempts = null;
    if (quizdata.timelimit === -1) quizdata.timelimit = null;
    let isIframe = req.query.iframe === "1";

    quizdata.otptimelimitdef = miscConfigs.otpExpiry;
    if (quizdata.otptimelimit === 0) quizdata.otptimelimit = miscConfigs.otpExpiry;

    let typeName;
    switch (type) {
        case 'info': typeName = 'Basic Information'; break;
        case 'date': typeName = 'Time Duration Settings'; break;
        case 'security': typeName = 'Security Settings'; break;
        case 'completion': typeName = 'Completion Settings'; break;
        default: res.redirect(`/course/info/${req.params.courseid}/${req.params.quizid}/overview?act=fut&edit=1`); break;
    }

    let options = {...renderConf, courseid: cid, editorFPRaw: `quiz/${cid}/${quizid}/`, quizid: quizid, data: quizdata, edittype: type,
        typeName: typeName, title: `Edit ${typeName} - ${quizdata.name}`};
    if (!isIframe) res.render('quiz/editshell', options);
    else res.render('quiz/editshelliframe', {...options, hideFooter: true});
});

function updateBasicInfo(quizdata, postdata) {
    quizdata.name = postdata.name;
    quizdata.description = (postdata.desc) ? postdata.desc : null;
    quizdata.totalscore = (postdata.quizTotalScore) ? parseInt(postdata.quizTotalScore) : null;
    quizdata.preinstructions = (postdata.instructions) ? postdata.instructions : null;
    quizdata.quiztype = parseInt(postdata.quizType);
    updateBasedOnQuizType(quizdata, quizdata.quiztype);
    return true;
}

function updateDate(quizdata, postdata, redirectUrl, res) {
    let enddate = (postdata.enddate) ? postdata.enddate : null;
    let sdchk = moment(new Date(postdata.startdate));
    let startDate = sdchk.format('YYYY-MM-DD HH:mm:ss');
    if (enddate) {
        let edchk = moment(new Date(enddate));
        enddate = edchk.format('YYYY-MM-DD HH:mm:ss');
        if (moment.duration(edchk.diff(sdchk)).asSeconds()<=0) {
            res.redirect(redirectUrl);
            return;
        }
    }

    quizdata.timestart = startDate;
    quizdata.timeend = enddate;
    quizdata.timelimit = (postdata.timelimit) ? parseInt(postdata.timelimitSec) : -1;
    quizdata.attempts = (postdata.attempts) ? parseInt(postdata.attempts) : -1;
    return true;
}

function updateSecurity(quizdata, postdata) {
    quizdata.password = (postdata.password) ? postdata.password : null;
    if (postdata.randomPassword) {
        quizdata.passwordgen = 1;
        quizdata.otptimelimit = (parseInt(postdata.otplimit) === 0) ? miscConfigs.otpExpiry : postdata.otplimit;
    } else quizdata.passwordgen = 0;

    return true;
}

function updateCompletion(quizdata, postdata) {
    quizdata.showgrade = (postdata.showgrade) ? parseInt(postdata.showgrade) : 0;
    quizdata.studentreport = (postdata.studentreport) ? parseInt(postdata.studentreport) : 0;
    return true;
}

// Update a specific feature of the quiz and return to overview page
router.post('/info/:courseid/:quizid/edit/:type', async (req, res) => {
    if (!(await canModifyThisCourseRed(res, req.params.courseid, false))) {res.redirect(`/course/info/${req.params.courseid}`); return; } // Kick out of editing mode

    let quizid = req.params.quizid;
    let cid = req.params.courseid;
    let type = req.params.type;
    let redisKey = `modifyquiz-${renderConf.loginId}-${cid}-${quizid}`;
    let quizdata = await cache.get(redisKey);
    quizdata = JSON.parse(quizdata);
    let postRes = false;

    // Update accordingly
    switch (type) {
        case 'info': postRes = updateBasicInfo(quizdata, req.body); break;
        case 'date': postRes = updateDate(quizdata, req.body, `/course/info/${req.params.courseid}/${req.params.quizid}/overview?act=fua&edit=1`, res); break;
        case 'security': postRes = updateSecurity(quizdata, req.body); break;
        case 'completion': postRes = updateCompletion(quizdata, req.body); break;
    }

    if (!postRes) return; // Error
    let isIframe = req.query.iframe === "1";

    try {
        await cache.setDefault(redisKey, JSON.stringify(quizdata));
        if (!isIframe) res.redirect(`/course/info/${req.params.courseid}/${req.params.quizid}/overview?act=suu&edit=1`);
        else res.json({success: true, redirect: `/course/info/${req.params.courseid}/${req.params.quizid}/overview?act=suu&edit=1`});
    } catch (err) {
        log.error(err);
        if (!isIframe) res.redirect(`/course/info/${req.params.courseid}/${req.params.quizid}/overview?act=fuu&edit=1`);
        else res.json({success: true, redirect: `/course/info/${req.params.courseid}/${req.params.quizid}/overview?act=fuu&edit=1`})
    }
});

// Delete a quiz
router.get('/info/:courseid/delete/:quizid', async (req, res) => {
    if (!(await canModifyThisCourseRed(res, req.params.courseid, false))) {res.redirect(`/course/info/${req.params.courseid}`); return; } // Kick out of editing mode
    try {
        let deleteQuizSQL = 'UPDATE quiz SET deleted=1 WHERE id=?';
        await db.query(deleteQuizSQL, [req.params.quizid]);
        res.redirect(`/course/info/${req.params.courseid}?edit=1&id=${req.params.quizid}&act=del`);
    } catch (err) {
        log.error(err);
        res.redirect(`/course/info/${req.params.courseid}?edit=1&id=${req.params.quizid}&act=delf`);
    }
});

// View quiz question management page
router.get('/info/:courseid/:quizid/questions', async (req, res) => {
    if (!(await canModifyThisCourseRed(res, req.params.courseid, false))) {res.redirect(`/course/info/${req.params.courseid}`); return; } // Kick out of editing mode
    let quizid = req.params.quizid;
    let cid = req.params.courseid;
    let redisKey = `modifyquiz-${renderConf.loginId}-${cid}-${quizid}`;

    let quizdata = await cache.get(redisKey);
    quizdata = JSON.parse(quizdata);

    let generalinfo = {name: quizdata.name};
    let sections = quizdata.sections;
    if (!sections) sections = {def: {name: "default", questions: []}}; // Set up default section
    else if (!sections.def) sections.def = {name: "default", questions: []};

    console.log(sections);

    let [qnBanks] = await db.query('SELECT * FROM questionsbank WHERE id IN (SELECT bankid FROM questionbank_access WHERE userid = ?) AND deleted=0', [renderConf.loginId]);

    res.render('quiz/qnmgmt', {...renderConf, courseid: cid, quizid: quizid, ginfo: generalinfo, sections: sections, banks: qnBanks, title: `Question Management - ${quizdata.name}`});
});

function getName(type) {
    switch (type) {
        case 1: return "Multiple-Choice";
        case 2: return "Short Structured";
        case 3: return "Code";
        case 4: return "Maths";
    }
}

// Add question into quiz
router.post('/info/:courseid/:quizid/questions/add', async (req, res) => {
    if (!(await canModifyThisCourseRed(res, req.params.courseid, false))) {res.redirect(`/course/info/${req.params.courseid}`); return; } // Kick out of editing mode
    let quizid = req.params.quizid;
    let cid = req.params.courseid;
    let redisKey = `modifyquiz-${renderConf.loginId}-${cid}-${quizid}`;
    let quizdata = await cache.get(redisKey);
    quizdata = JSON.parse(quizdata);

    let lang;
    if (req.body["languages[]"]) {
        if (Array.isArray(req.body["languages[]"])) lang = req.body["languages[]"].join(",");
        else lang = req.body["languages[]"];
    }

    let questionIds = req.body.qid.split(",");
    let [qninfo] = await db.query("SELECT q.id,q.name,q.maxscore,q.type,b.name AS bankname FROM question q JOIN questionsbank b on q.questionbankid = b.id WHERE q.id IN (?) AND q.deleted=0", [questionIds]);
    let questionsEmbed = [];
    for (let q of qninfo) {
        let qnPush = {id: q.id, name: q.name, score: q.maxscore, type: q.type, typeName: getName(q.type), bankname: q.bankname};
        if (q.type === 3) { // Code options
            let cInfo = (await db.query("SELECT difficulty FROM question_code WHERE questionid=?", [q.id]))[0][0];
            qnPush.opt = `${lang}|${cInfo.difficulty}`;
        }
        questionsEmbed.push(qnPush);
    }
    if (!quizdata.sections) quizdata.sections = {def: {name: "default", questions: []}};
    else if (!quizdata.sections.def) quizdata.sections.def = {name: "default", questions: []};

    if (req.body.section === "def") {
        quizdata.sections.def.questions.push(...questionsEmbed);
        quizdata.sections.def.count = quizdata.sections.def.questions.length;
    } else {
        let id = parseInt(req.body.section.split('-')[1]);
        quizdata.sections.list[id].questions.push(...questionsEmbed);
        quizdata.sections.list[id].count = quizdata.sections.list[id].questions.length;
    }
    await cache.setDefault(redisKey, JSON.stringify(quizdata));
    res.json({success: true});
});

// Delete question from quiz section in a quiz
router.delete('/info/:courseid/:quizid/questions/delete/:section/:qnid', async (req, res) => {
    if (!(await canModifyThisCourseRed(res, req.params.courseid, false))) {res.redirect(`/course/info/${req.params.courseid}`); return; } // Kick out of editing mode
    let quizid = req.params.quizid;
    let cid = req.params.courseid;
    let redisKey = `modifyquiz-${renderConf.loginId}-${cid}-${quizid}`;
    let quizdata = await cache.get(redisKey);
    quizdata = JSON.parse(quizdata);

    let idToDel = parseInt(req.params.qnid);
    let sectionToDel = req.params.section;

    if (sectionToDel === "def") {
        for (let [i,v] of quizdata.sections.def.questions.entries()) {
            if (v.id === idToDel) {
                quizdata.sections.def.questions.splice(i,1);
                break;
            }
        }
        quizdata.sections.def.count = quizdata.sections.def.questions.length;
    } else {
        sectionToDel = parseInt(sectionToDel);
        for (let [i,v] of quizdata.sections.list[sectionToDel].questions.entries()) {
            if (v.id === idToDel) {
                quizdata.sections.list[sectionToDel].questions.splice(i,1);
                break;
            }
        }
        quizdata.sections.list[sectionToDel].count = quizdata.sections.list[sectionToDel].questions.length;
    }

    await cache.setDefault(redisKey, JSON.stringify(quizdata));
    res.json({success: true});
});

// Add question section to a quiz
router.post('/info/:courseid/:quizid/questions/addSection', async (req, res) => {
    if (!(await canModifyThisCourseRed(res, req.params.courseid, false))) {res.redirect(`/course/info/${req.params.courseid}`); return; } // Kick out of editing mode
    let quizid = req.params.quizid;
    let cid = req.params.courseid;
    let redisKey = `modifyquiz-${renderConf.loginId}-${cid}-${quizid}`;
    let quizdata = await cache.get(redisKey);
    quizdata = JSON.parse(quizdata);
    let sectionName = req.body.sectionName;

    if (!quizdata.sections.hasOwnProperty("list")) quizdata.sections.list = [];

    quizdata.sections.list.push({name: sectionName, questions: [], count: 0, order: quizdata.sections.list.length, noofqns: -1});
    await cache.setDefault(redisKey, JSON.stringify(quizdata));
    res.json({success: true});
});

// Delete question section from a quiz
router.delete('/info/:courseid/:quizid/questions/delete/:sectionid', async (req, res) => {
    if (!(await canModifyThisCourseRed(res, req.params.courseid, false))) {res.redirect(`/course/info/${req.params.courseid}`); return; } // Kick out of editing mode
    let quizid = req.params.quizid;
    let cid = req.params.courseid;
    let redisKey = `modifyquiz-${renderConf.loginId}-${cid}-${quizid}`;
    let quizdata = await cache.get(redisKey);
    quizdata = JSON.parse(quizdata);

    let sectionIndex = req.params.sectionid;
    quizdata.sections.list.splice(sectionIndex,1);

    await cache.setDefault(redisKey, JSON.stringify(quizdata));
    res.json({success: true});
});

// Move reorder sections in a quiz
router.get('/info/:courseid/:quizid/questions/reorder/:from/:to', async (req, res) => {
    if (!(await canModifyThisCourseRed(res, req.params.courseid, false))) {res.redirect(`/course/info/${req.params.courseid}`); return; } // Kick out of editing mode
    let quizid = req.params.quizid;
    let cid = req.params.courseid;
    let redisKey = `modifyquiz-${renderConf.loginId}-${cid}-${quizid}`;
    let quizdata = await cache.get(redisKey);
    quizdata = JSON.parse(quizdata);

    let from = parseInt(req.params.from);
    let to = parseInt(req.params.to);

    let f = quizdata.sections.list.splice(from, 1)[0]; // remove `from` item and store it
    quizdata.sections.list.splice(to, 0, f); // insert stored item into position `to`

    await cache.setDefault(redisKey, JSON.stringify(quizdata));
    res.redirect(`/course/info/${cid}/${quizid}/questions`);
});

// Update common code languages for a section in the quiz
router.post('/info/:courseid/:quizid/questions/updateLang', async (req, res) => {
    if (!(await canModifyThisCourseRed(res, req.params.courseid, false))) {res.redirect(`/course/info/${req.params.courseid}`); return; } // Kick out of editing mode
    let quizid = req.params.quizid;
    let cid = req.params.courseid;
    let redisKey = `modifyquiz-${renderConf.loginId}-${cid}-${quizid}`;
    let quizdata = await cache.get(redisKey);
    quizdata = JSON.parse(quizdata);

    let lang, sid=req.body.sectionId;
    if (req.body["selOptions[]"]) {
        if (Array.isArray(req.body["selOptions[]"])) lang = req.body["selOptions[]"].join(",");
        else lang = req.body["selOptions[]"];
    }

    if (sid !== "def") {
        for (let q of quizdata.sections.list[parseInt(sid)].questions) {
            if (q.type !== 3) continue;
            let opts = q.opt.split("|");
            opts[0] = lang;
            q.opt = opts.join("|");
        }
    } else {
        for (let q of quizdata.sections.def.questions) {
            if (q.type !== 3) continue;
            let opts = q.opt.split("|");
            opts[0] = lang;
            q.opt = opts.join("|");
        }
    }

    await cache.setDefault(redisKey, JSON.stringify(quizdata));
    res.json({success: true});
});

// Update section options for a section in the quiz
router.post('/info/:courseid/:quizid/questions/updateOpts', async (req, res) => {
    if (!(await canModifyThisCourseRed(res, req.params.courseid, false))) {res.redirect(`/course/info/${req.params.courseid}`); return; } // Kick out of editing mode
    let quizid = req.params.quizid;
    let cid = req.params.courseid;
    let redisKey = `modifyquiz-${renderConf.loginId}-${cid}-${quizid}`;
    let quizdata = await cache.get(redisKey);
    quizdata = JSON.parse(quizdata);

    let sid=req.body.sectionId, numQns = parseInt(req.body.numQns);
    if (!numQns) numQns = -1;
    if (sid !== "def") quizdata.sections.list[parseInt(sid)].noofqns = numQns;
    else quizdata.sections.def.noofqns = numQns;

    await cache.setDefault(redisKey, JSON.stringify(quizdata));
    res.json({success: true});
});

// Move questions in a quiz between sections
router.post('/info/:courseid/:quizid/questions/moveQuestions', async (req, res) => {
    if (!(await canModifyThisCourseRed(res, req.params.courseid, false))) {res.redirect(`/course/info/${req.params.courseid}`); return; } // Kick out of editing mode
    let quizid = req.params.quizid;
    let cid = req.params.courseid;
    let redisKey = `modifyquiz-${renderConf.loginId}-${cid}-${quizid}`;
    let quizdata = await cache.get(redisKey);
    quizdata = JSON.parse(quizdata);

    let data = req.body;
    let oldIndex = parseInt(data.oldIndex), newIndex = parseInt(data.newIndex);

    // Remove from old list
    let tmp;
    if (data.oldSection === 'def') {
        tmp = quizdata.sections.def.questions[oldIndex];
        quizdata.sections.def.questions.splice(oldIndex, 1);
    }
    else {
        tmp = quizdata.sections.list[parseInt(data.oldSection)].questions[oldIndex];
        quizdata.sections.list[parseInt(data.oldSection)].questions.splice(oldIndex, 1);
    }

    // Move to new list
    if (data.newSection === 'def') quizdata.sections.def.questions.splice(newIndex, 0, tmp);
    else quizdata.sections.list[parseInt(data.newSection)].questions.splice(newIndex, 0, tmp);

    await cache.setDefault(redisKey, JSON.stringify(quizdata));
    res.json({success: true});
});

// Start Quiz
async function getQuizId(res, cid, qid) {
    if (!(await canAccessCourse(res, cid))) return null;
    let quizinfosql = "SELECT * FROM quiz WHERE id=? AND courseid=? AND deleted=0";
    let [data] = await db.query(quizinfosql, [qid, cid]);
    if (!data || data.length <= 0) { res.redirect(`/course/info/${cid}?act=ne`); return null; } // Quiz does not exist
    data = processQuizRecord(data[0], new Date());
    if (!data.isActive || data.isUnstarted) { res.redirect(`/course/info/${cid}?act=nea`); return null; }
    let props = ["studentreport", "showgrade", "deleted", "isActive", "isUnstarted", "creator", "quiztype"];
    for (let p of props) delete data[p]; // Remove unnecessary data
    return data;
}

// Start quiz page
router.get('/info/:courseid/:quizid/start', async (req, res) => {
    let cid = req.params.courseid, qid = req.params.quizid;
    let data = await getQuizId(res, cid, qid);
    if (!data) return;
    let canEdit = await canModifyThisCourseRed(res, req.params.courseid, false);
    data.password = (data.password); // Remove password being sent to client
    data.attemptcount = (await db.query("SELECT COUNT(id) AS count FROM quiz_attempt WHERE studentid=? AND quizid=?", [renderConf.loginId, qid]))[0][0];
    let [prevAttempts] = await db.query("SELECT id FROM quiz_attempt WHERE studentid=? AND quizid=? AND status=1 ORDER BY date DESC LIMIT 1", [renderConf.loginId, qid]);
    if (prevAttempts && prevAttempts.length > 0) data.prev = prevAttempts[0].id;
    else data.prev = null;
    let errmsg;
    if (req.query.correctpw) errmsg = "Wrong password for quiz entered";
    else if (req.query.invaliduser) errmsg = "Invalid user for this quiz attempt";
    else if (req.query.invalidattempt) errmsg = "Quiz Attempt has already been completed";
    else if (req.query.exceedattempts) errmsg = "You have reached the maximum number of attempts for this quiz";
    else if (req.query.noqn) errmsg = "No questions found in quiz";
    else if (req.query.noreport) errmsg = "Report not available for this quiz";
    else if (req.query.errorattempt) errmsg = "An error occurred processing this quiz attempt";
    let redirect = false;
    if (req.query.red) redirect = true;
    res.render('quiz/prequizinfo', {...renderConf, title: `Taking ${data.name}`, appName: "AASP", courseid: cid, quizid: qid, quiz: data, genPwApprove: canEdit, errmsg: errmsg, redirect: redirect});
});

async function generateQuestionsForAttempt(attemptId, quizid) {
    // Get sections for quiz
    let [sections] = await db.query("SELECT id, noofquestions FROM quiz_sections WHERE quizid=?", quizid);
    let draftQids = [];
    for (let section of sections) {
        // Get all questions from section
        let [questions] = await db.query("SELECT id FROM quiz_questions WHERE sectionid=? AND quizid=? AND deleted=0", [section.id, quizid]);
        let qids = [];
        for (let qn of questions) {
            qids.push(qn.id);
        }
        if (section.noofquestions < 0) draftQids = draftQids.concat(qids);
        else {
            if (section.noofquestions > qids.length) section.noofquestions = qids.length; // To account for the fact that we cannot pick more than needed
            appUtil.shuffleArray(qids);
            draftQids = draftQids.concat(qids.slice(0, section.noofquestions));
        }
    }

    // Format Draft QIDs to SQL format
    let questions = [];
    for (let q of draftQids) { questions.push([attemptId, q]); }

    if (questions.length > 0) await db.query("INSERT INTO quiz_attempt_qn (attemptid, questionid) VALUES ?", [questions]); // Insert to SQL
    await db.query("UPDATE quiz_attempt SET status=1 WHERE id=?", attemptId); // Update to done and start attempt
}

// Generate and start attempt if valid
router.post('/info/:courseid/:quizid/start', async (req, res) => {
    let cid = req.params.courseid, qid = req.params.quizid;
    let data = await getQuizId(res, cid, qid);
    if (!data) return;

    // Check if passworded quiz and check password match
    if (data.password) {if (!req.body.pw || req.body.pw !== data.password) {res.redirect(`/course/info/${cid}/${qid}/start?correctpw=1`);return;}}
    else if (data.passwordgen === 1) {
        // Check password against redis OTP if exists
        let redisPwKey = `quiz-${cid}-${qid}-pw`;
        let redisPW = await cache.get(redisPwKey);
        if (!redisPW || redisPW !== req.body.pw.toUpperCase()) {res.redirect(`/course/info/${cid}/${qid}/start?correctpw=1`);return;}
    }

    data.attemptcount = (await db.query("SELECT COUNT(id) AS count FROM quiz_attempt WHERE studentid=? AND quizid=?", [renderConf.loginId, qid]))[0][0];
    if (data.attempts !== "Unlimited" && data.attempts <= data.attemptcount.count) {res.redirect(`/course/info/${cid}/${qid}/start?exceedattempts=1`);return;}

    // Generate attempt with progress report (MySQL)
    let [result] = await db.query("INSERT INTO quiz_attempt (quizid, studentid) VALUES ?", [[[qid, renderConf.loginId]]]);

    generateQuestionsForAttempt(result.insertId, qid).then(r => log.info(`Questions for attempt #${result.insertId} generated successfully`));
    res.render('quiz/quizgenerationprogress', {...renderConf, title: 'Generating Quiz Questions', appName: "AASP", processKey: result.insertId, courseid: cid, quizid: qid});
});

// Generate OTP for a quiz
router.post('/info/:courseid/:quizid/genpw', async (req, res) => {
    let cid = req.params.courseid, qid = req.params.quizid;
    if (!(await canModifyThisCourseRed(res, cid, false))) {res.json({success: false}); return; } // Kick out of editing mode
    // Check if quiz pw
    let redisPwKey = `quiz-${cid}-${qid}-pw`;
    let existingData = await cache.get(redisPwKey);
    let forceRefresh = req.body.forceRefresh;
    let genpw, ttl;
    if (existingData && !forceRefresh) {
        genpw = existingData;
        ttl = await cache.ttl(redisPwKey);
    } else {
        // Generate PW and save it. PW is alphanumberic without symbols only
        genpw = pwgenerator.generate({
            length: miscConfigs.otpLength,
            numbers: true,
            lowercase: false,
            symbols: false
        });
        // Get expiry time
        let otptimedata = (await db.query("SELECT otptimelimit FROM quiz WHERE id=?", qid))[0][0];
        let otptime = miscConfigs.otpExpiry;
        if (otptimedata && otptimedata.otptimelimit !== 0) otptime = otptimedata.otptimelimit;
        await cache.set(redisPwKey, genpw, 'EX', otptime);
        ttl = otptime;
    }
    res.json({success: true, ttl: ttl, pw: genpw});
});


//Feedback page neccesity


// View quiz feedback management page
router.get('/info/:courseid/:quizid/feedbacks', async (req, res) => {
    if (!(await canModifyThisCourseRed(res, req.params.courseid, false))) {res.redirect(`/course/info/${req.params.courseid}`); return; } // Kick out of editing mode
    let quizid = req.params.quizid;
    let cid = req.params.courseid;
    let redisKey = `modifyquiz-${renderConf.loginId}-${cid}-${quizid}`;

    let quizdata = await cache.get(redisKey);
    quizdata = JSON.parse(quizdata);

    let generalinfo = {name: quizdata.name};
    let sections = quizdata.sections;
    if (!sections) sections = {def: {name: "default", feedbacks: []}}; // Set up default section
    else if (!sections.def) sections.def = {name: "default", feedbacks: []};

    console.log(sections);

    let [qnBanks] = await db.query('SELECT * FROM feedbacksbank WHERE id IN (SELECT bankid FROM feedbackbank_access WHERE userid = ?) AND deleted=0', [renderConf.loginId]);

    res.render('quiz/fbmgmt', {...renderConf, courseid: cid, quizid: quizid, ginfo: generalinfo, sections: sections, banks: qnBanks, title: `Feedback Management - ${quizdata.name}`});
});

function getName(type) {
    switch (type) {
        case 1: return "Multiple-Choice";
        case 2: return "Short Structured";
        case 3: return "Code";
        case 4: return "Maths";
    }
}

// Move feedbacks in a quiz between sections
router.post('/info/:courseid/:quizid/feedbacks/moveFeedbacks', async (req, res) => {
    if (!(await canModifyThisCourseRed(res, req.params.courseid, false))) {res.redirect(`/course/info/${req.params.courseid}`); return; } // Kick out of editing mode
    let quizid = req.params.quizid;
    let cid = req.params.courseid;
    let redisKey = `modifyquiz-${renderConf.loginId}-${cid}-${quizid}`;
    let quizdata = await cache.get(redisKey);
    quizdata = JSON.parse(quizdata);

    let data = req.body;
    let oldIndex = parseInt(data.oldIndex), newIndex = parseInt(data.newIndex);

    // Remove from old list
    let tmp;
    if (data.oldSection === 'def') {
        tmp = quizdata.sections.def.feedbacks[oldIndex];
        quizdata.sections.def.feedbacks.splice(oldIndex, 1);
    }
    else {
        tmp = quizdata.sections.list[parseInt(data.oldSection)].feedbacks[oldIndex];
        quizdata.sections.list[parseInt(data.oldSection)].feedbacks.splice(oldIndex, 1);
    }

    // Move to new list
    if (data.newSection === 'def') quizdata.sections.def.feedbacks.splice(newIndex, 0, tmp);
    else quizdata.sections.list[parseInt(data.newSection)].feedbacks.splice(newIndex, 0, tmp);

    await cache.setDefault(redisKey, JSON.stringify(quizdata));
    res.json({success: true});
});



// View quiz feedback management page
router.get('/info/:courseid/:quizid/feedbacks', async (req, res) => {
    if (!(await canModifyThisCourseRed(res, req.params.courseid, false))) {res.redirect(`/course/info/${req.params.courseid}`); return; } // Kick out of editing mode
    let quizid = req.params.quizid;
    let cid = req.params.courseid;
    let redisKey = `modifyquiz-${renderConf.loginId}-${cid}-${quizid}`;

    let quizdata = await cache.get(redisKey);
    quizdata = JSON.parse(quizdata);

    let generalinfo = {name: quizdata.name};
    let sections = quizdata.sections;
    if (!sections) sections = {def: {name: "default", feedbacks: []}}; // Set up default section
    else if (!sections.def) sections.def = {name: "default", feedbacks: []};

    console.log(sections);

    let [qnBanks] = await db.query('SELECT * FROM feedbacksbank WHERE id IN (SELECT bankid FROM feedbackbank_access WHERE userid = ?) AND deleted=0', [renderConf.loginId]);

    res.render('quiz/qnmgmt', {...renderConf, courseid: cid, quizid: quizid, ginfo: generalinfo, sections: sections, banks: qnBanks, title: `Feedback Management - ${quizdata.name}`});
});

function getName(type) {
    switch (type) {
        case 1: return "Multiple-Choice";
        case 2: return "Short Structured";
        case 3: return "Code";
        case 4: return "Maths";
    }
}

// Add feedback into quiz
router.post('/info/:courseid/:quizid/feedbacks/add', async (req, res) => {
    if (!(await canModifyThisCourseRed(res, req.params.courseid, false))) {res.redirect(`/course/info/${req.params.courseid}`); return; } // Kick out of editing mode
    let quizid = req.params.quizid;
    let cid = req.params.courseid;
    let redisKey = `modifyquiz-${renderConf.loginId}-${cid}-${quizid}`;
    let quizdata = await cache.get(redisKey);
    quizdata = JSON.parse(quizdata);

    let lang;
    if (req.body["languages[]"]) {
        if (Array.isArray(req.body["languages[]"])) lang = req.body["languages[]"].join(",");
        else lang = req.body["languages[]"];
    }

    let feedbackIds = req.body.qid.split(",");
    let [qninfo] = await db.query("SELECT q.id,q.name,q.maxscore,q.type,b.name AS bankname FROM feedback q JOIN feedbacksbank b on q.feedbackbankid = b.id WHERE q.id IN (?) AND q.deleted=0", [feedbackIds]);
    let feedbacksEmbed = [];
    for (let q of qninfo) {
        let qnPush = {id: q.id, name: q.name, score: q.maxscore, type: q.type, typeName: getName(q.type), bankname: q.bankname};
        if (q.type === 3) { // Code options
            let cInfo = (await db.query("SELECT difficulty FROM feedback_code WHERE feedbackid=?", [q.id]))[0][0];
            qnPush.opt = `${lang}|${cInfo.difficulty}`;
        }
        feedbacksEmbed.push(qnPush);
    }
    if (!quizdata.sections) quizdata.sections = {def: {name: "default", feedbacks: []}};
    else if (!quizdata.sections.def) quizdata.sections.def = {name: "default", feedbacks: []};
0
    await cache.setDefault(redisKey, JSON.stringify(quizdata));
    res.json({success: true});
});

// Delete feedback section from a quiz
router.delete('/info/:courseid/:quizid/feedbacks/delete/:sectionid', async (req, res) => {
    if (!(await canModifyThisCourseRed(res, req.params.courseid, false))) {res.redirect(`/course/info/${req.params.courseid}`); return; } // Kick out of editing mode
    let quizid = req.params.quizid;
    let cid = req.params.courseid;
    let redisKey = `modifyquiz-${renderConf.loginId}-${cid}-${quizid}`;
    let quizdata = await cache.get(redisKey);
    quizdata = JSON.parse(quizdata);

    let sectionIndex = req.params.sectionid;
    quizdata.sections.list.splice(sectionIndex,1);

    await cache.setDefault(redisKey, JSON.stringify(quizdata));
    res.json({success: true});
});

// Move reorder sections in a quiz
router.get('/info/:courseid/:quizid/feedbacks/reorder/:from/:to', async (req, res) => {
    if (!(await canModifyThisCourseRed(res, req.params.courseid, false))) {res.redirect(`/course/info/${req.params.courseid}`); return; } // Kick out of editing mode
    let quizid = req.params.quizid;
    let cid = req.params.courseid;
    let redisKey = `modifyquiz-${renderConf.loginId}-${cid}-${quizid}`;
    let quizdata = await cache.get(redisKey);
    quizdata = JSON.parse(quizdata);

    let from = parseInt(req.params.from);
    let to = parseInt(req.params.to);

    let f = quizdata.sections.list.splice(from, 1)[0]; // remove `from` item and store it
    quizdata.sections.list.splice(to, 0, f); // insert stored item into position `to`

    await cache.setDefault(redisKey, JSON.stringify(quizdata));
    res.redirect(`/course/info/${cid}/${quizid}/feedbacks`);
});

// Update common code languages for a section in the quiz
router.post('/info/:courseid/:quizid/feedbacks/updateLang', async (req, res) => {
    if (!(await canModifyThisCourseRed(res, req.params.courseid, false))) {res.redirect(`/course/info/${req.params.courseid}`); return; } // Kick out of editing mode
    let quizid = req.params.quizid;
    let cid = req.params.courseid;
    let redisKey = `modifyquiz-${renderConf.loginId}-${cid}-${quizid}`;
    let quizdata = await cache.get(redisKey);
    quizdata = JSON.parse(quizdata);

    let lang, sid=req.body.sectionId;
    if (req.body["selOptions[]"]) {
        if (Array.isArray(req.body["selOptions[]"])) lang = req.body["selOptions[]"].join(",");
        else lang = req.body["selOptions[]"];
    }

    if (sid !== "def") {
        for (let q of quizdata.sections.list[parseInt(sid)].feedbacks) {
            if (q.type !== 3) continue;
            let opts = q.opt.split("|");
            opts[0] = lang;
            q.opt = opts.join("|");
        }
    } else {
        for (let q of quizdata.sections.def.feedbacks) {
            if (q.type !== 3) continue;
            let opts = q.opt.split("|");
            opts[0] = lang;
            q.opt = opts.join("|");
        }
    }

    await cache.setDefault(redisKey, JSON.stringify(quizdata));
    res.json({success: true});
});

// Update section options for a section in the quiz
router.post('/info/:courseid/:quizid/feedbacks/updateOpts', async (req, res) => {
    if (!(await canModifyThisCourseRed(res, req.params.courseid, false))) {res.redirect(`/course/info/${req.params.courseid}`); return; } // Kick out of editing mode
    let quizid = req.params.quizid;
    let cid = req.params.courseid;
    let redisKey = `modifyquiz-${renderConf.loginId}-${cid}-${quizid}`;
    let quizdata = await cache.get(redisKey);
    quizdata = JSON.parse(quizdata);

    let sid=req.body.sectionId, numQns = parseInt(req.body.numQns);
    if (!numQns) numQns = -1;
    if (sid !== "def") quizdata.sections.list[parseInt(sid)].noofqns = numQns;
    else quizdata.sections.def.noofqns = numQns;

    await cache.setDefault(redisKey, JSON.stringify(quizdata));
    res.json({success: true});
});



module.exports = router;
