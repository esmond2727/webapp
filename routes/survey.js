const express = require('express');
const router = express.Router();
const auth = require('../lib/auth');
const appUtil = require('../lib/util');
const log = require('../lib/logger');

const db = require('../lib/db');
const cache = require('../lib/redis');
const { data } = require('jquery');
const { info } = require('npmlog');

const renderConf = {appName: 'Survey Management - AASP', route: 'Admin'};

router.use('*', async (req, res, next) => { if (await auth.initHeader(req, res, renderConf)) next(); });

// Route to list feedbacks by courses page
router.get('/listfbcourses', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'viewfeedbacks');
    res.render('survey/listfeedbacksbycourse', {...renderConf, title: 'Feedbacks by Courses'});
});
// Route to list feedbacks by quizzes page
router.get('/listfbcourses/:courseid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'viewfeedbacks');
    res.render('survey/listfeedbacksbyquiz', {...renderConf, title: 'Feedbacks by Quizzes', courseid: req.params.courseid});
});
// Route to list feedbacks page
router.get('/listfbquizzes/:quizid/', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'viewfeedbacks');
    let [quizname] = await db.query('SELECT quiz.name FROM quiz WHERE quiz.id = ? LIMIT 1', [req.params.quizid]);
    let [ratingone] = await db.query('SELECT COUNT(*) as count FROM feedbacks WHERE quizid = ? AND rating = 1', [req.params.quizid]);
    let [ratingtwo] = await db.query('SELECT COUNT(*) as count FROM feedbacks WHERE quizid = ? AND rating = 2', [req.params.quizid]);
    let [ratingthree] = await db.query('SELECT COUNT(*) as count FROM feedbacks WHERE quizid = ? AND rating = 3', [req.params.quizid]);
    let [ratingfour] = await db.query('SELECT COUNT(*) as count FROM feedbacks WHERE quizid = ? AND rating = 4', [req.params.quizid]);
    let [ratingfive] = await db.query('SELECT COUNT(*) as count FROM feedbacks WHERE quizid = ? AND rating = 5', [req.params.quizid]);
    let [score0] = await db.query('SELECT COUNT(*) as count FROM webassess.quiz_attempt where score >= 0 and score <= 9 and quizid - ?', [req.params.quizid]);
    let [score1] = await db.query('SELECT COUNT(*) as count FROM webassess.quiz_attempt where score >= 10 and score <= 19 and quizid - ?', [req.params.quizid]);
    let [score2] = await db.query('SELECT COUNT(*) as count FROM webassess.quiz_attempt where score >= 20 and score <= 29 and quizid - ?', [req.params.quizid]);
    let [score3] = await db.query('SELECT COUNT(*) as count FROM webassess.quiz_attempt where score >= 30 and score <= 39 and quizid - ?', [req.params.quizid]);
    let [score4] = await db.query('SELECT COUNT(*) as count FROM webassess.quiz_attempt where score >= 40 and score <= 49 and quizid - ?', [req.params.quizid]);
    let [score5] = await db.query('SELECT COUNT(*) as count FROM webassess.quiz_attempt where score >= 50 and score <= 59 and quizid - ?', [req.params.quizid]);
    let [score6] = await db.query('SELECT COUNT(*) as count FROM webassess.quiz_attempt where score >= 60 and score <= 69 and quizid - ?', [req.params.quizid]);
    let [score7] = await db.query('SELECT COUNT(*) as count FROM webassess.quiz_attempt where score >= 70 and score <= 79 and quizid - ?', [req.params.quizid]);
    let [score8] = await db.query('SELECT COUNT(*) as count FROM webassess.quiz_attempt where score >= 80 and score <= 89 and quizid - ?', [req.params.quizid]);
    let [score9] = await db.query('SELECT COUNT(*) as count FROM webassess.quiz_attempt where score >= 90 and score <= 100 and quizid - ?', [req.params.quizid]);
       let [avg] = await db.query('SELECT round(avg(score),2 )as avgscore FROM webassess.quiz_attempt where quizid - ?', [req.params.quizid]);
    
    
    
    
    
    
    
    
    
    
    let [course] = await db.query('SELECT courseid AS id FROM quiz WHERE id = ?', [req.params.quizid]);
    let courseid = course[0].id;
    let [studentsInCourse] = await db.query("SELECT id, userid FROM course_users WHERE courseid=? AND instructor=0", courseid);
    let noOfStudentsInCourse = studentsInCourse.length;
    let [studentsFeedback] = await db.query("SELECT DISTINCT fb.userid FROM feedbacks fb JOIN course_users cu ON cu.userid = fb.userid WHERE quizid = ? AND cu.instructor=0", [req.params.quizid]);
    let noOfStudentsFeedbacked = studentsFeedback.length;
    let noOfStudentsYetToFeedback = noOfStudentsInCourse - noOfStudentsFeedbacked;
    let feedbackPercent = ((noOfStudentsFeedbacked/noOfStudentsInCourse)*100).toFixed(2);
    let totalFeedbacks = ratingone[0].count + ratingtwo[0].count + ratingthree[0].count + ratingfour[0].count + ratingfive[0].count;
    let avgRating = ((1*ratingone[0].count)+(2*ratingtwo[0].count)+(3*ratingthree[0].count)+(4*ratingfour[0].count)+(5*ratingfive[0].count))/totalFeedbacks;
    avgRating = (avgRating).toFixed(1);
    res.render('survey/allfeedbacks', {...renderConf, title: 'Feedbacks by Quizzes', quizid: req.params.quizid, quizname: quizname[0].name, 
    ratingOne: ratingone[0].count, ratingTwo: ratingtwo[0].count, ratingThree: ratingthree[0].count, ratingFour: ratingfour[0].count, ratingFive: ratingfive[0].count,
    totalFeedbacks: totalFeedbacks, avgRating: avgRating, noOfStudentsInCourse: noOfStudentsInCourse, noOfStudentsFeedbacked: noOfStudentsFeedbacked, noOfStudentsYetToFeedback: noOfStudentsYetToFeedback,
    feedbackPercent: feedbackPercent,
    score0: score0[0].count, score1: score1[0].count, score2: score2[0].count, score3: score3[0].count, score4: score4[0].count, score5: score5[0].count, score6: score6[0].count, score7: score7[0].count, score8: score8[0].count, score9: score9[0].count, 
   avgscore:avg[0].avgscore


});
});

// Route to feedback details page
router.get('/listfbquizzes/:feedbackid/details', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'lectureranalytics');
    let [feedback] = await db.query('SELECT u.fullname, u.email AS email, fb.rating, fb.feedback FROM webassess.feedbacks fb JOIN webassess.users u ON u.id = fb.userid JOIN webassess.quiz q ON q.id = fb.quizid WHERE fb.id = ?', [req.params.feedbackid]);
    res.render('survey/feedbackdetail', {...renderConf, title: 'Feedback Detail', fullname: feedback[0].fullname, email: feedback[0].email, rating: feedback[0].rating, feedback: feedback[0].feedback});
});



// Submit Survey
router.post('/submit', async (req, res) => {
    try {
        appUtil.submitFeedback(req.body.userid, req.body.quizid, req.body.rating, req.body.feedback);
        return
    } catch (err) {
        log.error(err);
    }
});

// List all courses managed
router.get('/viewfbbycourses', async (req, res) => {
    let [mangedCourses] = await db.query('SELECT c.* FROM courses c WHERE c.id IN (SELECT cu.courseid FROM course_users cu WHERE userid = ? AND instructor = 1) AND deleted=0', [renderConf.loginId]);
    let data = [];
    for (let cc of mangedCourses) {
        data.push({id: cc.id, code: cc.code, name: cc.name, ts: cc.timestart, te: cc.timeend});
    }
    res.json({data: data});
});

// List all quizzes in course
router.get('/listfbcourses/:courseid/quizzes', async (req, res) => {
    let quizInfoSQL = 'SELECT * FROM quiz WHERE courseid=? AND deleted=0';
    let [quizlist] = await db.query(quizInfoSQL, [req.params.courseid]);
    let data = [];

    for (let quiz of quizlist) {
        let [feedbacksCount] = await db.query("SELECT COUNT(*) as counts FROM feedbacks WHERE quizid=?", [quiz.id]);
        data.push({id: quiz.id, courseid: quiz.courseid, name: quiz.name, desc: quiz.description, ts: quiz.timestart, te: quiz.timeend, feedbackscount: feedbacksCount[0].counts});
    }
    res.json({data: data});
});

// List all feedbacks in quiz
router.get('/listfbcourses/quizzes/:quizid', async (req, res) => {
    let [feedbacks] = await db.query('SELECT feedbacks.id, users.fullname, users.email, quiz.name as quiz, feedbacks.rating, feedbacks.feedback FROM webassess.feedbacks INNER JOIN webassess.users ON feedbacks.userid = users.id INNER JOIN webassess.quiz ON feedbacks.quizid = quiz.id WHERE feedbacks.quizid = ? ', [req.params.quizid]); //get feedback of students only *or dont tbc AND users.role = 4 
    let data = [];
    for (let feedback of feedbacks) {
        // let [feedbacksCount] = await db.query("SELECT COUNT(*) as counts FROM feedbacks WHERE quizid=?", [req.params.quizid]);
        data.push({id: feedback.id, username: feedback.fullname, email: feedback.email, quizname: feedback.quiz, rating: feedback.rating, feedback: feedback.feedback});
    }
    res.json({data: data});
});



module.exports = router;
