const express = require('express');
const router = express.Router();
const auth = require('../lib/auth');
const appUtil = require('../lib/util');
const log = require('../lib/logger');

const db = require('../lib/db');
const cache = require('../lib/redis');
const { data } = require('jquery');



const renderConf = {appName: 'Survey Management - AASP', route: 'Admin'};

router.use('*', async (req, res, next) => { if (await auth.initHeader(req, res, renderConf)) next(); });



// Access list Analytics page
router.get('/listkeywords', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'adminanalytics');
    res.render('analytics/listkeywords', {...renderConf, title: 'Feedbacks'});
});


// Access list rating counts by quiz page

router.get('/listratingcountsbyquiz', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'adminanalytics');
    res.render('analytics/listratingcountsbyquiz', {...renderConf, title: 'RatingCountsByQuiz'});
});

// Access listscorebystudentid
router.get('/listscorebystudentid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'adminanalytics');
    res.render('analytics/listscorebystudentid', {...renderConf, title: 'Listscorebystudentid'});
});

// Access listscorebyquizid
router.get('/listscorebyquizid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'adminanalytics');
    res.render('analytics/listscorebyquizid', {...renderConf, title: 'Listscorebyquizid'});
});

// Access listscorebyquestionid
router.get('/listscorebyquestionid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'adminanalytics');
    res.render('analytics/listscorebyquestionid', {...renderConf, title: 'Listscorebyquestionid'});
});
// Access listquizattemptbyquizid
router.get('/listquizattemptbyquizid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'adminanalytics');
    res.render('analytics/listquizattemptbyquizid', {...renderConf, title: 'Listquizattemptbyquizid'});
});
// Access liststudentanswerbyquestionid
router.get('/liststudentanswerbyquestionid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'adminanalytics');
    res.render('analytics/liststudentanswerbyquestionid', {...renderConf, title: 'Liststudentanswerbyquestionid'});
});



// Submit Survey
router.post('/submit', async (req, res) => {
    try {
        appUtil.submitFeedback(req.body.userid, req.body.quizid, req.body.rating, req.body.feedback);
        return
    } catch (err) {
        log.error(err);
    }
});

// List all feedbacks
router.get('/keyworddata', async (req, res) => {
    let success = '', failure = '';
    let [feedbacks] = await db.query(`SELECT SUM(total_count) as total, value
    FROM (
    
    SELECT count(*) AS total_count, REPLACE(REPLACE(REPLACE(x.value,'?',''),'.',''),'!','') as value
    FROM (
    SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(t.feedback, ' ', n.n), ' ', -1) value
      FROM webassess.feedbacks t CROSS JOIN 
    (
       SELECT a.N + b.N * 10 + 1 n
         FROM 
        (SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) a
       ,(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) b
        ORDER BY n
    ) n
     WHERE n.n <= 1 + (LENGTH(t.feedback) - LENGTH(REPLACE(t.feedback, ' ', '')))
     ORDER BY value
    
    ) AS x
    GROUP BY x.value
    
    ) AS y
    GROUP BY value`);
    let data = [];
    console.log(data + "1")
    for (let feedback of feedbacks) {
      

        data.push({ total: feedback.total, value: feedback.value});
    }
    res.json({data: data});
});

//access ratingcountdata
router.get('/ratingcountdata', async (req, res) => {
    let [ratingcounts] = await db.query(`SELECT quizattempt.id, users.fullname, users.email, quiz.name as quiz, feedbacks.rating, feedbacks.feedback ,quizattempt.score as score FROM webassess.feedbacks INNER JOIN webassess.users ON feedbacks.userid = users.id INNER JOIN webassess.quiz ON feedbacks.quizid = quiz.id inner join webassess.quiz_attempt quizattempt ON feedbacks.quizattemptid = quizattempt.id`);
    let data = [];
   
    for (let ratingcount of ratingcounts) {
      

        data.push({id: ratingcount.id, username: ratingcount.fullname, email: ratingcount.email, quizname: ratingcount.quiz, rating: ratingcount.rating, feedback: ratingcount.feedback, score: ratingcount.score});
    }
    res.json({data: data});
});

//access listratingcounts

router.get('/listratingcounts', async (req, res) => {
    let userrole = req.session.role
    console.log(userrole + "i love u" )
    auth.hasPermission(req, res, renderConf, 'adminanalytics');

    let [ratingone] = await db.query('SELECT COUNT(*) as count,COUNT(*) as count2 FROM feedbacks WHERE rating = 1');
    let [ratingtwo] = await db.query('SELECT COUNT(*) as count FROM feedbacks WHERE rating = 2');
    let [ratingthree] = await db.query('SELECT COUNT(*) as count FROM feedbacks WHERE rating = 3');
    let [ratingfour] = await db.query('SELECT COUNT(*) as count FROM feedbacks WHERE rating = 4');
    let [ratingfive] = await db.query('SELECT COUNT(*) as count FROM feedbacks WHERE rating = 5');
    let [score0] = await db.query('SELECT COUNT(*) as count FROM webassess.quiz_attempt where score >= 0 and  score <= 9;');
    let [score1] = await db.query('SELECT COUNT(*) as count FROM webassess.quiz_attempt where score >= 10 and  score <= 19;');
    let [score2] = await db.query('SELECT COUNT(*) as count FROM webassess.quiz_attempt where score >= 20 and  score <= 29;');
    let [score3] = await db.query('SELECT COUNT(*) as count FROM webassess.quiz_attempt where score >= 30 and  score <= 39;');   
    let [score4] = await db.query('SELECT COUNT(*) as count FROM webassess.quiz_attempt where score >= 40 and  score <= 49;');   
    let [score5] = await db.query('SELECT COUNT(*) as count FROM webassess.quiz_attempt where score >= 50 and  score <= 59;');   
    let [score6] = await db.query('SELECT COUNT(*) as count FROM webassess.quiz_attempt where score >= 60 and  score <= 69;');   
    let [score7] = await db.query('SELECT COUNT(*) as count FROM webassess.quiz_attempt where score >= 70 and  score <= 79;');
    let [score8] = await db.query('SELECT COUNT(*) as count FROM webassess.quiz_attempt where score >= 80 and  score <= 89;');   
    let [score9] = await db.query('SELECT COUNT(*) as count FROM webassess.quiz_attempt where score >= 90 and  score <= 100;');   
    let [avg] = await db.query('SELECT round(avg(score),2 )as avgscore FROM webassess.quiz_attempt');
    
    // let [course] = await db.query('SELECT courseid AS id FROM quiz WHERE id = 1');
    // let courseid = course[0].id;
    // let [studentsInCourse] = await db.query("SELECT id, userid FROM course_users WHERE courseid=1 AND instructor=0", courseid);
    // let noOfStudentsInCourse = studentsInCourse.length;
    // let [studentsFeedback] = await db.query("SELECT DISTINCT fb.userid FROM feedbacks fb JOIN course_users cu ON cu.userid = fb.userid WHERE quizid = 1 AND cu.instructor=0");
    // let noOfStudentsFeedbacked = studentsFeedback.length;
    // let noOfStudentsYetToFeedback = noOfStudentsInCourse - noOfStudentsFeedbacked;
    // let feedbackPercent = ((noOfStudentsFeedbacked/noOfStudentsInCourse)*100).toFixed(2);
    let totalFeedbacks = ratingone[0].count + ratingtwo[0].count + ratingthree[0].count + ratingfour[0].count + ratingfive[0].count;
    let avgRating = ((1*ratingone[0].count)+(2*ratingtwo[0].count)+(3*ratingthree[0].count)+(4*ratingfour[0].count)+(5*ratingfive[0].count))/totalFeedbacks;
    avgRating = (avgRating).toFixed(1);

    res.render('analytics/listratingcounts', {...renderConf, title: 'RatingCounts',ratingOne: ratingone[0].count, ratingTwo: ratingtwo[0].count, ratingThree: ratingthree[0].count, ratingFour: ratingfour[0].count, ratingFive: ratingfive[0].count,
    totalFeedbacks: totalFeedbacks, avgRating: avgRating,
    score0: score0[0].count, score1: score1[0].count, score2: score2[0].count, score3: score3[0].count, score4: score4[0].count, score5: score5[0].count, score6: score6[0].count, score7: score7[0].count, score8: score8[0].count, score9: score9[0].count, 
    avgscore:avg[0].avgscore
  
  
  
  
  
  
  
    //noOfStudentsInCourse: noOfStudentsInCourse, noOfStudentsFeedbacked: noOfStudentsFeedbacked, noOfStudentsYetToFeedback: noOfStudentsYetToFeedback,
    // feedbackPercent: feedbackPercen
});
});





//access ratingcountbyquizdata
router.get('/ratingcountbyquizdata', async (req, res) => {
    let success = '', failure = '';
    let [ratingcounts] = await db.query(`SELECT distinct COUNT( rating) as count,rating,quizid FROM webassess.feedbacks group by quizid,rating;`);
    let data = [];
    for (let ratingcount of ratingcounts) {
      

        data.push({ count: ratingcount.count, rating: ratingcount.rating, quizid: ratingcount.quizid});
    }
    res.json({data: data});
});

//access scorequestioniddata
router.get('/scorequestioniddata', async (req, res) => {
    let success = '', failure = '';
    let [scorequestioniddatas] = await db.query(`SELECT avg(score) as avg ,min(score) as min,max(score) as max,questionid FROM webassess.quiz_attempt_qn group by questionid;
    `);
    let data = [];
    console.log(data + "1")
    for (let scorequestioniddata of scorequestioniddatas) {
      

        data.push({ avg: scorequestioniddata.avg, min: scorequestioniddata.min , max: scorequestioniddata.max,questionid: scorequestioniddata.questionid});
    }
    res.json({data: data});
});

//access scorequiziddata
router.get('/scorequiziddata', async (req, res) => {
    let success = '', failure = '';
    let [scorequiziddatas] = await db.query(`SELECT avg(score) as avg,min(score) as min,max(score) as max,quizid FROM webassess.quiz_attempt group by quizid;`);
    let data = [];
    console.log(data + "1")
    for (let scorequiziddata of scorequiziddatas) {
      

        data.push({ avg: scorequiziddata.avg, min: scorequiziddata.min, max: scorequiziddata.max,quizid: scorequiziddata.quizid});
    }
    res.json({data: data});
});

//access scorestudentiddata
router.get('/scorestudentiddata', async (req, res) => {
    let success = '', failure = '';
    let [scorestudentiddatas] = await db.query(`SELECT avg(score) as avg,min(score) as min,max(score) as max,studentid FROM webassess.quiz_attempt group by studentid;
    `);
    let data = [];
    console.log(data + "1")
    for (let scorestudentiddata of scorestudentiddatas) {
      

        data.push({ avg: scorestudentiddata.avg, min: scorestudentiddata.min, max: scorestudentiddata.max, studentid: scorestudentiddata.studentid});
    }
    res.json({data: data});
});

//access quizattemptdata
router.get('/quizattemptdata', async (req, res) => {
    let success = '', failure = '';
    let [quizattemptdatas] = await db.query(`SELECT count(quiz) as count,quiz FROM webassess.quiz_attempt_count group by quiz`);
    let data = [];
    console.log(data + "1")
    for (let quizattemptdata of quizattemptdatas) {
      

        data.push({ count: quizattemptdata.count, quizid: quizattemptdata.quiz});
    }
    res.json({data: data});
});


//access studentanswerbyquestioniddata
router.get('/studentanswerbyquestioniddata', async (req, res) => {
    let success = '', failure = '';
    let [studentanswerbyquestioniddatas] = await db.query(`SELECT qa.questionid, qa.answer,count(qa.answer) as count,qa.score FROM webassess.quiz_attempt_qn qa JOIN webassess.quiz_questions qq on qa.questionid = qq.id JOIN webassess.question q on qq.questionid = q.id JOIN webassess.quiz_sections qs on qq.sectionid = qs.id group by qa.questionid,qa.answer,qa.score;`);
    let data = [];
    console.log(data + "1")
    for (let studentanswerbyquestioniddata of studentanswerbyquestioniddatas) {
      

        data.push({ questionid: studentanswerbyquestioniddata.questionid, count: studentanswerbyquestioniddata.count, answer: studentanswerbyquestioniddata.answer, score: studentanswerbyquestioniddata.score,});
    }
    res.json({data: data});
});
module.exports = router;


//access loginactivitydata
router.get('/loginactivitydata', async (req, res) => {
    let success = '', failure = '';
    let [loginactivitydatas] = await db.query(`select * from logs;`);
    let data = [];
    console.log(data + "1")
    for (let loginactivitydata of loginactivitydatas) {
      

        data.push({ username: loginactivitydata.username, salt: loginactivitydata.salt,
            password: loginactivitydata.password, ip: loginactivitydata.ip, mac: loginactivitydata.mac,
            sessionid: loginactivitydata.sessionid,logintimestamp: loginactivitydata.logintimestamp,
            logouttimestamp: loginactivitydata.logouttimestamp,
            country: loginactivitydata.country,
            countrycode: loginactivitydata.countrycode,
            region: loginactivitydata.region,
            regionname: loginactivitydata.regionname,
            city: loginactivitydata.city,
            zip: loginactivitydata.zip,
            latitude: loginactivitydata.latitude,
            longtitude: loginactivitydata.longtitude,
            timezone: loginactivitydata.timezone,
            isp: loginactivitydata.isp,
            organisation: loginactivitydata.organisation,
            autonomoussystem: loginactivitydata.autonomoussystem,

        
        
        
        
        
        
        
        
        });
    }
    res.json({data: data});
});


//access loginactivity
router.get('/loginactivity', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'labanalytics');

    //login count by year
    let [year1] = await db.query('SELECT COUNT(*) as count FROM logs WHERE year(logintimestamp) = year(CURRENT_DATE())');
    let [year2] = await db.query('SELECT COUNT(*) as count FROM logs WHERE year(logintimestamp) = YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR))');
    let [year3] = await db.query('SELECT COUNT(*) as count FROM logs WHERE year(logintimestamp) = YEAR(DATE_SUB(CURDATE(), INTERVAL 2 YEAR))');
    let [year4] = await db.query('SELECT COUNT(*) as count FROM logs WHERE year(logintimestamp) = YEAR(DATE_SUB(CURDATE(), INTERVAL 3 YEAR))');
    let [year5] = await db.query('SELECT COUNT(*) as count FROM logs WHERE year(logintimestamp) = YEAR(DATE_SUB(CURDATE(), INTERVAL 4 YEAR))');

    //login count by month

    let [month1] = await db.query('SELECT COUNT(*) as count FROM logs WHERE month(logintimestamp) = month(CURRENT_DATE())');
    let [month2] = await db.query('SELECT COUNT(*) as count FROM logs WHERE month(logintimestamp) = month((DATE_SUB(CURDATE(), INTERVAL 1 month)))');
    let [month3] = await db.query('SELECT COUNT(*) as count FROM logs WHERE month(logintimestamp) = month((DATE_SUB(CURDATE(), INTERVAL 2 month)))');
    let [month4] = await db.query('SELECT COUNT(*) as count FROM logs WHERE month(logintimestamp) = month((DATE_SUB(CURDATE(), INTERVAL 3 month)))');
    let [month5] = await db.query('SELECT COUNT(*) as count FROM logs WHERE month(logintimestamp) = month((DATE_SUB(CURDATE(), INTERVAL 4 month)))');
    let [month6] = await db.query('SELECT COUNT(*) as count FROM logs WHERE month(logintimestamp) = month((DATE_SUB(CURDATE(), INTERVAL 5 month)))');
    let [month7] = await db.query('SELECT COUNT(*) as count FROM logs WHERE month(logintimestamp) = month((DATE_SUB(CURDATE(), INTERVAL 6 month)))');
    let [month8] = await db.query('SELECT COUNT(*) as count FROM logs WHERE month(logintimestamp) = month((DATE_SUB(CURDATE(), INTERVAL 7 month)))');
    let [month9] = await db.query('SELECT COUNT(*) as count FROM logs WHERE month(logintimestamp) = month((DATE_SUB(CURDATE(), INTERVAL 8 month)))');
    let [month10] = await db.query('SELECT COUNT(*) as count FROM logs WHERE month(logintimestamp) = month((DATE_SUB(CURDATE(), INTERVAL 9 month)))');
    let [month11] = await db.query('SELECT COUNT(*) as count FROM logs WHERE month(logintimestamp) = month((DATE_SUB(CURDATE(), INTERVAL 10 month)))');
    let [month12] = await db.query('SELECT COUNT(*) as count FROM logs WHERE month(logintimestamp) = month((DATE_SUB(CURDATE(), INTERVAL 11 month)))');
     

    //login count by day

    let [day1] = await db.query('SELECT COUNT(*) as count FROM logs WHERE day(logintimestamp) = day(CURRENT_DATE())');
    let [day2] = await db.query('SELECT COUNT(*) as count FROM logs WHERE day(logintimestamp) = day((DATE_SUB(CURDATE(), INTERVAL 1 day)))');
    let [day3] = await db.query('SELECT COUNT(*) as count FROM logs WHERE day(logintimestamp) = day((DATE_SUB(CURDATE(), INTERVAL 2 day)))');
    let [day4] = await db.query('SELECT COUNT(*) as count FROM logs WHERE day(logintimestamp) = day((DATE_SUB(CURDATE(), INTERVAL 3 day)))');
    let [day5] = await db.query('SELECT COUNT(*) as count FROM logs WHERE day(logintimestamp) = day((DATE_SUB(CURDATE(), INTERVAL 4 day)))');
    let [day6] = await db.query('SELECT COUNT(*) as count FROM logs WHERE day(logintimestamp) = day((DATE_SUB(CURDATE(), INTERVAL 5 day)))');
    let [day7] = await db.query('SELECT COUNT(*) as count FROM logs WHERE day(logintimestamp) = day((DATE_SUB(CURDATE(), INTERVAL 6 day)))');
    let [day8] = await db.query('SELECT COUNT(*) as count FROM logs WHERE day(logintimestamp) = day((DATE_SUB(CURDATE(), INTERVAL 7 day)))');
    let [day9] = await db.query('SELECT COUNT(*) as count FROM logs WHERE day(logintimestamp) = day((DATE_SUB(CURDATE(), INTERVAL 8 day)))');
    let [day10] = await db.query('SELECT COUNT(*) as count FROM logs WHERE day(logintimestamp) = day((DATE_SUB(CURDATE(), INTERVAL 9 day)))');
    let [day11] = await db.query('SELECT COUNT(*) as count FROM logs WHERE day(logintimestamp) = day((DATE_SUB(CURDATE(), INTERVAL 10 day)))');
    let [day12] = await db.query('SELECT COUNT(*) as count FROM logs WHERE day(logintimestamp) = day((DATE_SUB(CURDATE(), INTERVAL 11 day)))');
    let [day13] = await db.query('SELECT COUNT(*) as count FROM logs WHERE day(logintimestamp) = day((DATE_SUB(CURDATE(), INTERVAL 12 day)))');
    let [day14] = await db.query('SELECT COUNT(*) as count FROM logs WHERE day(logintimestamp) = day((DATE_SUB(CURDATE(), INTERVAL 13 day)))');

    //login count by semester1
    let [sem1aug] = await db.query('SELECT count(*) as count FROM logs WHERE month(logintimestamp) = 8');
    let [sem1sep] = await db.query('SELECT count(*) as count FROM logs WHERE month(logintimestamp) = 9');
    let [sem1oct] = await db.query('SELECT count(*) as count FROM logs WHERE month(logintimestamp) = 10');
    let [sem1nov] = await db.query('SELECT count(*) as count FROM logs WHERE month(logintimestamp) = 11');

    //login count by semester2

    let [sem2jan] = await db.query('SELECT count(*) as count FROM logs WHERE month(logintimestamp) = 1');
    let [sem2feb] = await db.query('SELECT count(*) as count FROM logs WHERE month(logintimestamp) = 2');
    let [sem2mar] = await db.query('SELECT count(*) as count FROM logs WHERE month(logintimestamp) = 3');
    let [sem2apr] = await db.query('SELECT count(*) as count FROM logs WHERE month(logintimestamp) = 4');

    //hour spent from 12am to 11pm
    let [hrspent0] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 0 AND HOUR(l.logintimestamp) < 1');
    let [hrspent1] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 1 AND HOUR(l.logintimestamp) < 2');
    let [hrspent2] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 2 AND HOUR(l.logintimestamp) < 3');
    let [hrspent3] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 3 AND HOUR(l.logintimestamp) < 4');
    let [hrspent4] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 4 AND HOUR(l.logintimestamp) < 5');
    let [hrspent5] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 5 AND HOUR(l.logintimestamp) < 6');
    let [hrspent6] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 6 AND HOUR(l.logintimestamp) < 7');
    let [hrspent7] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 7 AND HOUR(l.logintimestamp) < 8');
    let [hrspent8] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 8 AND HOUR(l.logintimestamp) < 9');
    let [hrspent9] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 9 AND HOUR(l.logintimestamp) < 10');
    let [hrspent10] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 10 AND HOUR(l.logintimestamp) < 11');
    let [hrspent11] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 11 AND HOUR(l.logintimestamp) < 12');
    let [hrspent12] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 12 AND HOUR(l.logintimestamp) < 13');
    let [hrspent13] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 13 AND HOUR(l.logintimestamp) < 14');
    let [hrspent14] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 14 AND HOUR(l.logintimestamp) < 15');
    let [hrspent15] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 15 AND HOUR(l.logintimestamp) < 16');
    let [hrspent16] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 16 AND HOUR(l.logintimestamp) < 17');
    let [hrspent17] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 17 AND HOUR(l.logintimestamp) < 18');
    let [hrspent18] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 18 AND HOUR(l.logintimestamp) < 19');
    let [hrspent19] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 19 AND HOUR(l.logintimestamp) < 20');
    let [hrspent20] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 20 AND HOUR(l.logintimestamp) < 21');
    let [hrspent21] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 21 AND HOUR(l.logintimestamp) < 22');
    let [hrspent22] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 22 AND HOUR(l.logintimestamp) < 23');
    let [hrspent23] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 23');

   
    //hour spent base on hours calculation from minuteseconds
    let [hrspenthr0] = await db.query('SELECT count(*) as count from logs where round(TIMESTAMPDIFF(SECOND, logintimestamp, logouttimestamp)/360) = 0 and  logouttimestamp is not null');
    let [hrspenthr1] = await db.query('SELECT count(*) as count from logs where round(TIMESTAMPDIFF(SECOND, logintimestamp, logouttimestamp)/360) = 1 and  logouttimestamp is not null');
    let [hrspenthr2] = await db.query('SELECT count(*) as count from logs where round(TIMESTAMPDIFF(SECOND, logintimestamp, logouttimestamp)/360) = 2 and  logouttimestamp is not null');
    let [hrspenthr3] = await db.query('SELECT count(*) as count from logs where round(TIMESTAMPDIFF(SECOND, logintimestamp, logouttimestamp)/360) = 3 and  logouttimestamp is not null');
    let [hrspenthr4] = await db.query('SELECT count(*) as count from logs where round(TIMESTAMPDIFF(SECOND, logintimestamp, logouttimestamp)/360) = 4 and  logouttimestamp is not null');
    let [hrspenthr5] = await db.query('SELECT count(*) as count from logs where round(TIMESTAMPDIFF(SECOND, logintimestamp, logouttimestamp)/360) = 5 and  logouttimestamp is not null');
    let [hrspenthr6] = await db.query('SELECT count(*) as count from logs where round(TIMESTAMPDIFF(SECOND, logintimestamp, logouttimestamp)/360) = 6 and  logouttimestamp is not null');
    let [hrspenthr7] = await db.query('SELECT count(*) as count from logs where round(TIMESTAMPDIFF(SECOND, logintimestamp, logouttimestamp)/360) = 7 and  logouttimestamp is not null');
    let [hrspenthr8] = await db.query('SELECT count(*) as count from logs where round(TIMESTAMPDIFF(SECOND, logintimestamp, logouttimestamp)/360) = 8 and  logouttimestamp is not null');
    let [hrspenthr9] = await db.query('SELECT count(*) as count from logs where round(TIMESTAMPDIFF(SECOND, logintimestamp, logouttimestamp)/360) = 9 and  logouttimestamp is not null');
    let [hrspenthr10] = await db.query('SELECT count(*) as count from logs where round(TIMESTAMPDIFF(SECOND, logintimestamp, logouttimestamp)/360) = 10 and  logouttimestamp is not null');
    let [hrspenthr11] = await db.query('SELECT count(*) as count from logs where round(TIMESTAMPDIFF(SECOND, logintimestamp, logouttimestamp)/360) = 11 and  logouttimestamp is not null');
    let [hrspenthr12] = await db.query('SELECT count(*) as count from logs where round(TIMESTAMPDIFF(SECOND, logintimestamp, logouttimestamp)/360) = 12 and  logouttimestamp is not null');
    let [hrspenthr13] = await db.query('SELECT count(*) as count from logs where round(TIMESTAMPDIFF(SECOND, logintimestamp, logouttimestamp)/360) = 13 and  logouttimestamp is not null');
    let [hrspenthr14] = await db.query('SELECT count(*) as count from logs where round(TIMESTAMPDIFF(SECOND, logintimestamp, logouttimestamp)/360) = 14 and  logouttimestamp is not null');
    let [hrspenthr15] = await db.query('SELECT count(*) as count from logs where round(TIMESTAMPDIFF(SECOND, logintimestamp, logouttimestamp)/360) = 15 and  logouttimestamp is not null');
    let [hrspenthr16] = await db.query('SELECT count(*) as count from logs where round(TIMESTAMPDIFF(SECOND, logintimestamp, logouttimestamp)/360) = 16 and  logouttimestamp is not null');
    let [hrspenthr17] = await db.query('SELECT count(*) as count from logs where round(TIMESTAMPDIFF(SECOND, logintimestamp, logouttimestamp)/360) = 17 and  logouttimestamp is not null');
    let [hrspenthr18] = await db.query('SELECT count(*) as count from logs where round(TIMESTAMPDIFF(SECOND, logintimestamp, logouttimestamp)/360) = 18 and  logouttimestamp is not null');
    let [hrspenthr19] = await db.query('SELECT count(*) as count from logs where round(TIMESTAMPDIFF(SECOND, logintimestamp, logouttimestamp)/360) = 19 and  logouttimestamp is not null');
    let [hrspenthr20] = await db.query('SELECT count(*) as count from logs where round(TIMESTAMPDIFF(SECOND, logintimestamp, logouttimestamp)/360) = 20 and  logouttimestamp is not null');
    let [hrspenthr21] = await db.query('SELECT count(*) as count from logs where round(TIMESTAMPDIFF(SECOND, logintimestamp, logouttimestamp)/360) = 21 and  logouttimestamp is not null');
    let [hrspenthr22] = await db.query('SELECT count(*) as count from logs where round(TIMESTAMPDIFF(SECOND, logintimestamp, logouttimestamp)/360) = 22 and  logouttimestamp is not null');
    let [hrspenthr23] = await db.query('SELECT count(*) as count from logs where round(TIMESTAMPDIFF(SECOND, logintimestamp, logouttimestamp)/360) = 23 and  logouttimestamp is not null');
    let [hrspenthr24] = await db.query('SELECT count(*) as count from logs where round(TIMESTAMPDIFF(SECOND, logintimestamp, logouttimestamp)/360) >= 24 and  logouttimestamp is not null');
   
    //login count by day of week 7 = saturday 1 = sunday
    let [dayofweek1] = await db.query('select count(*) as count from logs where DAYOFWEEK(logintimestamp) = 1');
    let [dayofweek2] = await db.query('select count(*) as count from logs where DAYOFWEEK(logintimestamp) = 2');
    let [dayofweek3] = await db.query('select count(*) as count from logs where DAYOFWEEK(logintimestamp) = 3');
    let [dayofweek4] = await db.query('select count(*) as count from logs where DAYOFWEEK(logintimestamp) = 4');
    let [dayofweek5] = await db.query('select count(*) as count from logs where DAYOFWEEK(logintimestamp) = 5');
    let [dayofweek6] = await db.query('select count(*) as count from logs where DAYOFWEEK(logintimestamp) = 6');
    let [dayofweek7] = await db.query('select count(*) as count from logs where DAYOFWEEK(logintimestamp) = 7');

    //hour spent by day of day midnight 12am-6am morning 6am-12pm afternoon 12pm-6pm night 6pm-12am
    let [midnight] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 0 AND HOUR(l.logintimestamp) < 6');
    let [morning] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 6 AND HOUR(l.logintimestamp) < 12');
    let [afternoon] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 12 AND HOUR(l.logintimestamp) < 18');
    let [night] = await db.query('SELECT count(*) as count FROM logs l WHERE HOUR(l.logintimestamp) >= 18 AND HOUR(l.logintimestamp) < 24');

   
   
   
    res.render('analytics/loginactivity', {...renderConf, title: 'LoginActivity',
    year1: year1[0].count, year2: year2[0].count, year3: year3[0].count, year4: year4[0].count, year5: year5[0].count,

    month1: month1[0].count, month2: month2[0].count, month3: month3[0].count, month4: month4[0].count,
    month5: month5[0].count, month6: month6[0].count, month7: month7[0].count, month8: month8[0].count, 
    month9: month9[0].count, month10: month10[0].count, month11: month11[0].count, month12: month12[0].count,
    
    day1: day1[0].count, day2: day2[0].count, day3: day3[0].count, day4: day4[0].count, day5: day5[0].count, 
    day6: day6[0].count, day7: day7[0].count, day8: day8[0].count, day9: day9[0].count, day10: day10[0].count, 
    day11: day11[0].count, day12: day12[0].count,day13: day13[0].count, day14: day14[0].count,
    
    sem1aug: sem1aug[0].count,sem1sep: sem1sep[0].count,sem1oct: sem1oct[0].count,sem1nov: sem1nov[0].count,
    sem2jan: sem2jan[0].count,sem2feb: sem2feb[0].count,sem2mar: sem2mar[0].count,sem2apr: sem2apr[0].count,
  
    hrspent0: hrspent0[0].count, hrspent1: hrspent1[0].count, hrspent2: hrspent2[0].count, hrspent3: hrspent3[0].count,
    hrspent4: hrspent4[0].count, hrspent5: hrspent5[0].count, hrspent6: hrspent6[0].count, hrspent7: hrspent7[0].count,
    hrspent8: hrspent8[0].count, hrspent9: hrspent9[0].count, hrspent10: hrspent10[0].count, hrspent11: hrspent11[0].count,
    hrspent12: hrspent12[0].count, hrspent13: hrspent13[0].count, hrspent14: hrspent14[0].count, hrspent15: hrspent15[0].count, 
    hrspent16: hrspent16[0].count, hrspent17: hrspent17[0].count, hrspent18: hrspent18[0].count, hrspent19: hrspent19[0].count,
    hrspent20: hrspent20[0].count, hrspent21: hrspent21[0].count, hrspent22: hrspent22[0].count, hrspent23: hrspent23[0].count,
  
    hrspenthr0: hrspenthr0[0].count, hrspenthr1: hrspenthr1[0].count, hrspenthr2: hrspenthr2[0].count, hrspenthr3: hrspenthr3[0].count,
    hrspenthr4: hrspenthr4[0].count, hrspenthr5: hrspenthr5[0].count, hrspenthr6: hrspenthr6[0].count, hrspenthr7: hrspenthr7[0].count,
    hrspenthr8: hrspenthr8[0].count, hrspenthr9: hrspenthr9[0].count, hrspenthr10: hrspenthr10[0].count, hrspenthr11: hrspenthr11[0].count,
    hrspenthr12: hrspenthr12[0].count, hrspenthr13: hrspenthr13[0].count, hrspenthr14: hrspenthr14[0].count, hrspenthr15: hrspenthr15[0].count, 
    hrspenthr16: hrspenthr16[0].count, hrspenthr17: hrspenthr17[0].count, hrspenthr18: hrspenthr18[0].count, hrspenthr19: hrspenthr19[0].count,
    hrspenthr20: hrspenthr20[0].count, hrspenthr21: hrspenthr21[0].count, hrspenthr22: hrspenthr22[0].count, hrspenthr23: hrspenthr23[0].count,
    hrspenthr24: hrspenthr24[0].count,
  
    dayofweek1: dayofweek1[0].count, dayofweek2: dayofweek2[0].count, dayofweek3: dayofweek3[0].count, dayofweek4: dayofweek4[0].count,
    dayofweek5: dayofweek5[0].count, dayofweek6: dayofweek6[0].count, dayofweek7: dayofweek7[0].count,

    morning: morning[0].count, afternoon: afternoon[0].count, night: night[0].count, midnight: midnight[0].count
    //noOfStudentsInCourse: noOfStudentsInCourse, noOfStudentsFeedbacked: noOfStudentsFeedbacked, noOfStudentsYetToFeedback: noOfStudentsYetToFeedback,
    // feedbackPercent: feedbackPercen
});
});

module.exports = router;