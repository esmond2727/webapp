const express = require('express');
const router = express.Router();
const auth = require('../lib/auth');
const {miscConfigs} = require('../config')
const log = require('../lib/logger');

const cache = require('../lib/redis');
const db = require('../lib/db');
const moment = require('moment');
const humanizeDuration = require('humanize-duration');
const csv = require('fast-csv');
const appUtil = require('../lib/util');

const qnMethods = require('./questions');

const renderConf = {appName: 'Taking Quiz - AASP'};

router.use('*', async (req, res, next) => { if (await auth.initHeader(req, res, renderConf)) next(); });

// Get status of quiz attempt generation
router.get('/:key/genstatus', async (req, res) => {
    let quizid = req.params.key;
    let info = (await db.query("SELECT id, quizid, studentid, date, status FROM quiz_attempt WHERE id=?", [quizid]))[0][0];
    switch (info.status) {
        case 0: info.statusMsg = 'Generating Questions for Quiz...'; break;
        case 1: info.statusMsg = "Questions finished generating. Redirecting..."; break;
        case 2: info.statusMsg = "Quiz Attempt queued and awaiting to be marked..."; break;
        case 3: info.statusMsg = "Marking Quiz Attempt..."; break;
        case 4: info.statusMsg = "Quiz Attempt marked successfully. Redirecting..."; break;
        case 5: info.statusMsg = "An error occurred. Redirecting..."; break;
        default: info.statusMsg = "Unknown Status for this stage"; break;
    }
    res.json(info);
});

async function getTimeLeft(quizid) {
    let attemptData = (await db.query("SELECT qa.date, q.timelimit FROM quiz_attempt qa JOIN quiz q on qa.quizid = q.id WHERE qa.id=?", quizid))[0][0];
    if (attemptData.timelimit === -1) {
        attemptData.timeLeftMillis = -1;
        attemptData.timeLeftSecs = -1
    } else {
        let startDate = new Date(attemptData.date), endDate = new Date(startDate.getTime()), curDate = new Date();
        endDate.setSeconds(endDate.getSeconds() + attemptData.timelimit);
        attemptData.timeLeftMillis = endDate - curDate;
        attemptData.timeLeftSecs = Math.ceil(attemptData.timeLeftMillis / 1000);
    }
    return attemptData;
}

// Get time left in quiz attempt
router.get('/:key/timeleft', async (req, res) => {
    let quizid = req.params.key;
    let attemptData = await getTimeLeft(quizid);
    res.json(attemptData);
})

// Get question for a quiz attempt
router.get('/:key/qn/:qno', async (req, res) => {
    let quizid = req.params.key, qno = req.params.qno;
    let studentchk = (await db.query("SELECT qa.quizid, qa.studentid, q.courseid, qa.status FROM quiz_attempt qa JOIN quiz q ON q.id = qa.quizid WHERE qa.id=?", quizid))[0][0];
    if (!studentchk || typeof studentchk === 'undefined' || studentchk.studentid !== renderConf.loginId) {res.redirect(`/course/info/${studentchk.courseid}/${studentchk.quizid}/start?invaliduser=1`); return;}
    if (studentchk.status !== 1) {res.redirect(`/course/info/${studentchk.courseid}/${studentchk.quizid}/start?invalidattempt=1`); return;}
    let qnSQL = "SELECT qa.id, qa.questionid, qq.sectionid, qs.name, qq.questionid as qnid, q.name, qq.opt, qa.answer, qa.usersubmit FROM quiz_attempt_qn qa JOIN quiz_questions qq on qa.questionid = qq.id JOIN question q on qq.questionid = q.id JOIN quiz_sections qs on qq.sectionid = qs.id WHERE qa.attemptid=?";
    let quiz_attempt_qn = (await db.query("SELECT questionid FROM quiz_attempt_qn WHERE attemptid=?", quizid))[0][0];
    let [questions] = await db.query(qnSQL, quizid);
    let attemptData = await getTimeLeft(quizid);    
    let attemptCountsToday = (await db.query("SELECT count(*) AS user_submission_today FROM quiz_attempt_count WHERE submission_date >= CURDATE() AND quiz = ? AND questionid = ? AND studentid = ?", [studentchk.quizid, quiz_attempt_qn.questionid, renderConf.loginId]))[0][0];
    attemptCountsToday = attemptCountsToday.user_submission_today;
    if (attemptData.timeLeftMillis < 0 && attemptData.timelimit > 0) {
        // Time's already up for this attempt
        res.redirect(`/attempt/${quizid}/submit?overtime=1`);
        return;
    }
    let qindex = qno - 1;
    if (qindex >= questions.length || qindex < 0) {
        // Try move back to qn 1 if it is not qn 1 already
        if (qno !== '1') {
            res.redirect(`/attempt/${quizid}/qn/1`)
        } else {
            await db.query("UPDATE quiz_attempt SET status=5 WHERE id=?", quizid); // Set as error
            res.redirect(`/course/info/${studentchk.courseid}/${studentchk.quizid}/start?noqn=1`)
        }
        return;
    }
    let curQn = questions[qindex];
    let qdata = await qnMethods.accessQnInfo(curQn.qnid);
    let opt = {...renderConf, qlist: questions, title: `Q${qno}`, data: qdata, curQnNo: qno, attemptid: quizid, timeLeft: attemptData.timeLeftSecs, attemptCounts: attemptCountsToday};
    switch (qdata.type) {
        case 1:
            opt.selectedAns = curQn.answer;
            break;
        case 2:
            opt.answer = curQn.answer;
            break;
        case 3:
            opt.testcount = qdata.code.testCount;
            opt.enabledLangs = curQn.opt.split("|")[0].split(",");
            opt.codeedit = qdata.code.editorList;

            let isprimary
            for (let lang of qdata.code.templates) {
                if (!opt.enabledLangs.includes(lang.type)) continue;
                if (!isprimary) isprimary = lang.type;
                if (lang.isprimary) isprimary = lang.type;
            }
            opt.prilang = isprimary;
            let [codeData] = await db.query("SELECT language, filename FROM quiz_attempt_code WHERE attemptid=? LIMIT 1", curQn.id);
            if (codeData && codeData.length > 0) {
                opt.existingcode = JSON.stringify({code: curQn.answer, language: codeData[0].language, filename: codeData[0].filename});
            } else {
                opt.existingcode = null;
            }
            break;
    }
    res.render('quiz/qnshell', opt);
});

// Submit question of a quiz attempt
router.post('/:key/qn/:qno', async (req, res) => {
    let quizid = req.params.key, qno = req.params.qno, qindex = qno - 1;
    log.debug('Quiz ID: %d, Qn No: %d', quizid, qno);
    log.debug(req.body);
    let qnSQL = "SELECT qa.id, qa.questionid, qq.sectionid, qs.name, qq.questionid as qnid, q.name, qq.opt, qa.answer, qa.usersubmit FROM quiz_attempt_qn qa JOIN quiz_questions qq on qa.questionid = qq.id JOIN question q on qq.questionid = q.id JOIN quiz_sections qs on qq.sectionid = qs.id WHERE qa.attemptid=?";
    let [questions] = await db.query(qnSQL, quizid);
    if (qindex >= questions.length || qindex < 0) {res.json({success: true});return;} // Pretend submitted
    let curQn = questions[qindex];
    let qdata = await qnMethods.accessQnInfo(curQn.qnid);
    let usubmit = 0;
    if (req.body.usersubmit && req.body.usersubmit === "true") usubmit = 1;
    let dbConn = await db.getConnection();
    try {
        await dbConn.beginTransaction();
        let submitState = (await dbConn.query("SELECT usersubmit FROM quiz_attempt_qn WHERE attemptid=? AND questionid=?", [quizid, curQn.questionid]))[0][0];
        if (submitState && submitState.usersubmit === 1) usubmit = 1; // Already submitted just keep it there
        let ansToSave = "";

        // Question type specific code saving
        switch (qdata.type) {
            case 1:
                ansToSave = req.body.selAnsInfo;
                break;
            case 2:
                ansToSave = req.body.structAns;
                break;
            case 3:
                let codedata = JSON.parse(req.body.code);
                ansToSave = codedata.code;
                let fn = req.body.filename;
                if (!fn || fn === "") {
                    // First find from template, get default filename as last resort
                    fn = null;
                    for (let t of qdata.code.templates) {
                        if (t.type === codedata.language) {
                            fn = (!t.filename || t.filename === "") ? null : t.filename;
                            break;
                        }
                    }
                    if (fn === null) {
                        // Get from templates itself
                        let [template] = await dbConn.query("SELECT defaultFilename FROM code_language WHERE templatekey=? LIMIT 1", codedata.language);
                        fn = template[0].defaultFilename;
                    }
                }
                await dbConn.query("DELETE FROM quiz_attempt_code WHERE attemptid=?", curQn.id);
                await dbConn.query("REPLACE INTO quiz_attempt_code(attemptid, language, filename) VALUES ?", [[[curQn.id, codedata.language, fn]]]);
                break;
        }
        let newQnData = [ansToSave, usubmit, quizid, curQn.questionid];
        await dbConn.query("UPDATE quiz_attempt_qn SET answer=?, usersubmit=? WHERE attemptid=? AND questionid=?", newQnData); // Update answer
        await dbConn.commit();
        let quiz_attempt_qn = (await db.query("SELECT id, questionid FROM quiz_attempt_qn WHERE attemptid=? AND questionid=?", [quizid, curQn.questionid]))[0][0];
        let actualQuizid = (await db.query("SELECT quizid FROM quiz_attempt WHERE id=?", quizid))[0][0];
        let DATE_FORMATER = require('dateformat');
        let datetimeNow = DATE_FORMATER( new Date(), "yyyy-mm-dd HH:MM:ss");
        await db.query("INSERT INTO quiz_attempt_count (quiz_attempt_id, quiz, questionid, studentid, submission_date) VALUES ?", [[[quiz_attempt_qn.id, actualQuizid.quizid, quiz_attempt_qn.questionid, renderConf.loginId, datetimeNow]]]);
        res.json({success: true});
    } catch (err) {
        log.error(err);
        await dbConn.rollback();
        res.json({success: false});
    } finally {
        await dbConn.release();
    }
});

// get attempt count of question on page load
router.post('/:key/qn/:qno/getAttemptCounts', async (req, res) => {
    let quizid = req.params.key, qno = req.params.qno, qindex = qno - 1;
    let qnSQL = "SELECT qa.id, qa.questionid, qq.sectionid, qs.name, qq.questionid as qnid, q.name, qq.opt, qa.answer, qa.usersubmit FROM quiz_attempt_qn qa JOIN quiz_questions qq on qa.questionid = qq.id JOIN question q on qq.questionid = q.id JOIN quiz_sections qs on qq.sectionid = qs.id WHERE qa.attemptid=?";
    let [questions] = await db.query(qnSQL, quizid);
    if (qindex >= questions.length || qindex < 0) {res.json({success: true});return;} // Pretend submitted
    let curQn = questions[qindex];  
    //NOTE: quizid is attemptid, curQn.questionid is quiz_attempt_qn's questionid, qnid is the id of the question from question table
    let quiz_attempt_qn = (await db.query("SELECT id, questionid FROM quiz_attempt_qn WHERE attemptid=? AND questionid=?", [quizid, curQn.questionid]))[0][0];
    let questionid = quiz_attempt_qn.questionid;
    let question_attempt_limit = (await db.query("SELECT submission_limit FROM question WHERE id=?", [curQn.qnid]))[0][0];
    question_attempt_limit = question_attempt_limit.submission_limit;

    let qa_quizid = (await db.query("SELECT quizid FROM quiz_attempt WHERE id = ?", quizid))[0][0];
    qa_quizid = qa_quizid.quizid;
    let attemptCountsToday = (await db.query("SELECT count(*) AS user_submission_today FROM quiz_attempt_count WHERE submission_date >= CURDATE() AND quiz = ? AND questionid = ? AND studentid = ?", [qa_quizid, questionid, renderConf.loginId]))[0][0];
    attemptCountsToday = attemptCountsToday.user_submission_today;
    log.info("attemptCountsToday: "+attemptCountsToday);
    let dbConn = await db.getConnection();
    try {
        res.json({success: true, counts: attemptCountsToday, limit: question_attempt_limit});
    } catch (err) {
        log.error(err);
        await dbConn.rollback();
        res.json({success: false});
    } finally {
        await dbConn.release();
    }
});

function getHHMMSSString(sec) {
    if (sec <= 0) return "00:00:00";
    let hrs = Math.floor(sec / 3600), min = Math.floor((sec - (hrs * 3600)) / 60), seconds = sec - (hrs * 3600) - (min * 60);
    seconds = Math.round(seconds * 100) / 100
    return (hrs < 10 ? "0" + hrs : hrs) + " hours, " + (min < 10 ? "0" + min : min) + " minutes, " + (seconds < 10 ? "0" + seconds : seconds) + " seconds";
}

// Submit quiz attempt
router.get("/:key/submit", async (req, res) => {
    let quizattemptid = req.params.key;
    log.debug(quizattemptid);
    let studentchk = (await db.query("SELECT qa.quizid, qa.studentid, q.courseid, qa.status FROM quiz_attempt qa JOIN quiz q ON q.id = qa.quizid WHERE qa.id=?", quizattemptid))[0][0];
    if (!studentchk || typeof studentchk === 'undefined' || studentchk.studentid !== renderConf.loginId) {res.redirect(`/course/info/${studentchk.courseid}/${studentchk.quizid}/start?invaliduser=1`); return;}
    if (studentchk.status !== 1) {res.redirect(`/course/info/${studentchk.courseid}/${studentchk.quizid}/start?invalidattempt=1`); return;}
    // Set as submitted
    await db.query("UPDATE quiz_attempt SET status=2, submitteddate=current_timestamp() WHERE id=?", quizattemptid); // Set as pending marking
    //originally:
    //let sparedata = (await db.query("SELECT qa.submitteddate, qa.id, q.showgrade, q.studentreport, qa.date AS startdate, q.name AS quizname, c.name AS coursename, c.code, q.courseid FROM quiz_attempt qa JOIN quiz q on qa.quizid = q.id JOIN courses c on q.courseid = c.id WHERE qa.id=?", quizattemptid))[0][0];
    let sparedata = (await db.query("SELECT qa.submitteddate, qa.id AS quizattemptid, q.id AS quizid, q.showgrade, q.studentreport, qa.date AS startdate, q.name AS quizname, c.name AS coursename, c.code, q.courseid FROM quiz_attempt qa JOIN quiz q on qa.quizid = q.id JOIN courses c on q.courseid = c.id WHERE qa.id=?", quizattemptid))[0][0];
    let enddate = new Date(sparedata.submitteddate), startdate = new Date(sparedata.startdate);
    let duration = Math.ceil(Math.abs(enddate - startdate) / 1000);
    sparedata.durationString = getHHMMSSString(duration);
    let errdata;
    if (req.query.overtime) errdata = "Your time for this quiz have expired and the system has autosubmitted your quiz for you";
    await cache.rpush(miscConfigs.submissionQueue, `s-${quizattemptid}`);
    res.render('quiz/quizsubmitted', {...renderConf, title: "Quiz Submitted!", submissionData: sparedata, errmsg: errdata});
});


// Generate report for students if show report/grade after attempt is enabled
router.get("/:key/getReportStudentAttempt", async (req, res) => {
    let quizattempt = req.params.key;
    let studentchk = (await db.query("SELECT qa.quizid, qa.studentid, q.courseid, qa.status FROM quiz_attempt qa JOIN quiz q ON q.id = qa.quizid WHERE qa.id=?", quizattempt))[0][0];
    if (!studentchk || typeof studentchk === 'undefined' || studentchk.studentid !== renderConf.loginId) {res.redirect(`/course/info/${studentchk.courseid}/${studentchk.quizid}/start?invaliduser=1`); return;}
    // Make sure that we can actually allow students to get report result
    let quizchk = (await db.query("SELECT showgrade, studentreport FROM quiz WHERE id=?", studentchk.quizid))[0][0];
    if (!quizchk || typeof quizchk === 'undefined') {res.redirect(`/course/info/${studentchk.courseid}/${studentchk.quizid}/start?noreport=1`); return;}
    if (quizchk.showgrade === 0 && quizchk.studentreport === 0) {res.redirect(`/course/info/${studentchk.courseid}/${studentchk.quizid}/start?noreport=1`); return;}
    // Check status and redirect or render necessarily
    log.debug(studentchk);
    if (studentchk.status === 3) {
        // Render to mark page
        res.render('quiz/quizawaitmarking', {...renderConf, title: 'Marking Quiz Attempt', appName: "AASP", attemptid: quizattempt, cid: studentchk.courseid, qid: studentchk.quizid});
    } else if (studentchk.status === 4) {
        res.redirect(`/reports/view/${quizattempt}/reportSA`); // Redirect to marked page
    } else {
        res.redirect(`/course/info/${studentchk.courseid}/${studentchk.quizid}/start?errorattempt=1`); // Error occurred
    }
});

module.exports = router;