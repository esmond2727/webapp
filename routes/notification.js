const express = require('express');
const router = express.Router();
const auth = require('../lib/auth');
const appUtil = require('../lib/util');
const log = require('../lib/logger');

const db = require('../lib/db');
const cache = require('../lib/redis');
const csv = require('fast-csv');

const renderConf = {appName: 'User Management - AASP', route: 'Admin'};

router.use('*', async (req, res, next) => { if (await auth.initHeader(req, res, renderConf)) next(); });



// List notification
router.get('/list', async (req, res) => {
    let sucmsg = ""
    if (req.query.success && parseInt(req.query.success)) {
        let getusernameSQL = "SELECT emailfreq, username FROM notification WHERE id=?";
        let [username] = await db.query(getusernameSQL, [req.query.success]);
        sucmsg = `User ${username[0].username} has been updated successfully!`;
    } 
    res.render('notification/listuser', {...renderConf, title: 'User List', sucmsg: sucmsg});
});

// Get data of all notification
router.get('/userdata', async (req, res) => {
    let sess = req.session;
    let power = sess.rolePower;
    let user = sess.username;
    let [notification] = await db.query(`select n.id ,n.username,c.cronfreq  from notification n join cronschedule c on n.emailfreq = c.id where n.username =  "${user}"`, [power]);
    let data = [];
    for (let n of notification) {
        log.debug(n);
        data.push({id: n.id,username: n.username, emailfreq: n.cronfreq,});
    }
    res.json({data: data});
});

// Edit user page
router.get('/edit/:userid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'edituser');
    let [departments] = await db.query('SELECT * FROM departments');
    let [cronschedule] = await db.query('SELECT * FROM cronschedule'); // Do not show SA
    let getUserListSQL = 'SELECT * FROM notification WHERE id=?';
    let user = (await db.query(getUserListSQL, [req.params.userid]))[0][0];
    delete user.password;
    delete user.salt; // Don't pass this to the user
    log.debug(user);
    res.render('notification/edituser', {...renderConf, title: `Editing ${user.username}`, user: user, editUserFullName: user.emailfreq, departments: departments, cronschedule: cronschedule})
});

// Edit a user
router.post('/edit/:userid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'edituser');
    let query = `UPDATE notification SET emailfreq=? WHERE id=?`;
    let data = [ req.body.emailfreq];
    data.push(req.params.userid);
    await db.query(query, data);
    console.log(query)
    console.log("apple")
    console.log(data)
    res.redirect(`/changepassword`);
});



module.exports = router;
