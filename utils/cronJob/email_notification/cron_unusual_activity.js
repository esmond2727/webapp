const cron = require("node-cron");
const moment = require("moment");
var mysql = require('mysql');

const smtpTransporter = require("../helper/mail.js");
const { emailWithAttachment } = require("../helper/emailHelper.js");

const excel = require("exceljs");

// array list so that email will be store here
const dailyemaillistarray = [];
const weeklyemaillistarray = [];
const monthlyemaillistarray = [];
const yearlyemaillistarray = [];

// array list so that data will be store here
const dailyunusualdataarray = [];
const weeklyunusualdataarray = [];
const monthlyunusualdataarray = [];
const yearlyunusualdataarray = [];

//cron schedule
const dailyunusualcronschedule = "* 00 * * *";
const weeklyunusualcronschedule = "00 00 * * 01";
const monthlyunusualcronschedule = "00 00 01 * *";
const yearlyunusualcronschedule = "00 00 01 01 *";

//some moment implementation for datetime 
const yesterday = moment().subtract(1, "days").format("YYYY-MM-DD 00:00:00");
const today = moment().format("YYYY-MM-DD 00:00:00");
const weeklyend = moment().subtract(8, "days").format("YYYY-MM-DD 00:00:00");
const monthlyend = moment().subtract(1, "month").format("YYYY-MM-DD 00:00:00");
const yearlyend = moment().subtract(1, "year").format("YYYY-MM-DD 00:00:00");

const date = moment().format("YYYYMMDD");


        var con = mysql.createConnection({
          
                host: process.env.MYSQL_HOST,
                user: process.env.MYSQL_USER,
                password: process.env.MYSQL_PASSWORD,
                database: process.env.MYSQL_DATABASE,
            //     host: process.env.SS_MYSQL_HOST,
            // user: process.env.SS_MYSQL_USER,
            // password: process.env.SS_MYSQL_PASSWORD,
            // database: process.env.SS_MYSQL_DATABASE,
            
             });
        con.connect(function(err) {
            if (err) throw err;
            console.log("Connected!");
            var sql = "SELECT email FROM users u join notification n on u.username = n.username join cronschedule c on c.id = n.emailfreq where cronfreq = 'Daily';";
            con.query(sql, function (err, result1) {
                if (err) throw err;
                for (let i = 0; i < result1.length; i++) {
                    dailyemaillistarray.push(result1[i].email)
                }
            })
            var sql2 = "SELECT email FROM users u join notification n on u.username = n.username join cronschedule c on c.id = n.emailfreq where cronfreq = 'Weekly';";
            con.query(sql2, function (err, result2) {
                if (err) throw err;
                for (let i = 0; i < result2.length; i++) {
                    weeklyemaillistarray.push(result2[i].email)
                }
            })
            var sql3 = "SELECT email FROM users u join notification n on u.username = n.username join cronschedule c on c.id = n.emailfreq where cronfreq = 'Monthly';";
            con.query(sql3, function (err, result3) {
                if (err) throw err;
                for (let i = 0; i < result3.length; i++) {
                    monthlyemaillistarray.push(result3[i].email)
                }
            })
            var sql4 = "SELECT email FROM users u join notification n on u.username = n.username join cronschedule c on c.id = n.emailfreq where cronfreq = 'Yearly';";
            con.query(sql4, function (err, result4) {
                if (err) throw err;
                for (let i = 0; i < result4.length; i++) {
                    yearlyemaillistarray.push(result4[i].email)
                }
            })
           
            var sql5 = `SELECT username,country,ip,logintimestamp from logs where (country is null or country != "Singapore") and date(logintimestamp)  = "${yesterday}"`;
            console.log(sql5)
            con.query(sql5, function (err, result5) {
                if (err) throw err;
                for (let i = 0; i < result5.length; i++) {
                    result5[i].logintimestamp = moment(result5[i].logintimestamp).add(0, "hours").format("YYYY-MM-DD HH:mm:ss")

                    dailyunusualdataarray.push(result5[i])
                    
                }
            })
         
            var sql6 = `SELECT username,country,ip, logintimestamp from logs where (country is null or country != "Singapore") and date(logintimestamp) < "${today}" and  date(logintimestamp) > "${weeklyend}"`;
            console.log(sql6)
            con.query(sql6, function (err, result6) {
                
                if (err) throw err;
                for (let i = 0; i < result6.length; i++) {
                    result6[i].logintimestamp = moment(result6[i].logintimestamp).add(0, "hours").format("YYYY-MM-DD HH:mm:ss")
                    weeklyunusualdataarray.push(result6[i])

                }
            })
           
            var sql7 = `SELECT username,country,ip, logintimestamp from logs where (country is null or country != "Singapore") and date(logintimestamp) < "${today}" and  date(logintimestamp) >= "${monthlyend}"`;
            console.log(sql7)
            con.query(sql7, function (err, result7) {
                
                if (err) throw err;
                for (let i = 0; i < result7.length; i++) {
                    result7[i].logintimestamp = moment(result7[i].logintimestamp).add(0, "hours").format("YYYY-MM-DD HH:mm:ss")
                    monthlyunusualdataarray.push(result7[i])

                }
            })
            var sql8 = `SELECT username,country,ip, logintimestamp from logs where (country is null or country != "Singapore") and date(logintimestamp) < "${today}" and  date(logintimestamp) >= "${yearlyend}"`;
            console.log(sql8)
            con.query(sql8, function (err, result8) {
                
                if (err) throw err;
                for (let i = 0; i < result8.length; i++) {
                    result8[i].logintimestamp = moment(result8[i].logintimestamp).add(0, "hours").format("YYYY-MM-DD HH:mm:ss")
                    yearlyunusualdataarray.push(result8[i])

                }
            })

            //query example var sql9 = ` SELECT ip,count(*) as count from logs where  logintimestamp <  "2022-02-21 11:22:47" and  logintimestamp >= "2022-02-21 11:21:47" group by ip`;
            
               // var sql6 = `SELECT ip,count( *) as count from logs where  logintimestamp <  "${now}" and  logintimestamp >= "${minago}" group by ip`;
            
        })
      





const unusualactivityemail = async (recipientarray, titleofduration) => {

    let header = [
        {
            header: "Username",
            key: "username",
            width: 10,
        },
        {
            header: "Country",
            key: "country",
            width: 30,
        },
        {
            header: "IP Address",
            key: "ip",
            width: 30,
        },
        {
            header: "Login Time",
            key: "logintimestamp",
            width: 30,
        },
    ];
    //creating workbook
    let workbook2 = new excel.Workbook({
        useStyles: true,
    }); 
    //creating worksheet
    let worksheet = workbook2.addWorksheet("plan_report"); 


     worksheet.columns = header;

    worksheet.duplicateRow(1, 1, true); 
    //duplicate header row 2 times with values and style; in row2 and row3
    worksheet.mergeCells("A1:D1");

         worksheet.getCell("A01").value = `${titleofduration}${process.env.EMAIL_TITLE_UNUSUAL_ACTIVITY}`;
    if(titleofduration == "Daily"){
        worksheet.addRows(dailyunusualdataarray);
    }
    else if(titleofduration == "Weekly"){
        worksheet.addRows(weeklyunusualdataarray);
    }
    else if(titleofduration == "Monthly"){
        worksheet.addRows(monthlyunusualdataarray);
    }
    else if(titleofduration == "Yearly"){
        worksheet.addRows(yearlyunusualdataarray);
    }
//filtering in excel
worksheet.autoFilter = {
    from: "A2",
    to: "D2",
};
//alignment and setup in excel

worksheet.columns.forEach((column) => {
    column.border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" },
    };
    column.alignment = {
        wrapText: true,
        vertical: "top",
        horizontal: "left",
    };
});
worksheet.pageSetup.fitToPage = true;
worksheet.pageSetup.margins = {
    left: 1.8,
    right: 1.8,
    top: 0.9,
    bottom: 0.9,
    header: 0.8,
    footer: 0.8,
};
    filename = `${date}_${titleofduration}_Unusual_Activity.xlsx`;

    // Write to File
    workbook2.xlsx
        .writeFile(`cron_files/${filename}`)
        .then(function () {
            console.log("file saved!");
  

    title = titleofduration + process.env.EMAIL_TITLE_UNUSUAL_ACTIVITY;

    recipient = recipientarray;

    smtpTransporter.sendMail(
        emailWithAttachment(title, filename, recipient),
        function (error, info) {
            if (error) {
                console.log(error);
            } else {
                console.log("Email sent: " + info.response);
            }
        }
    );
});
};




cron.schedule(dailyunusualcronschedule, () => unusualactivityemail(dailyemaillistarray, "Daily"));
cron.schedule(weeklyunusualcronschedule, () => unusualactivityemail(weeklyemaillistarray, "Weekly"));
cron.schedule(monthlyunusualcronschedule, () => unusualactivityemail(monthlyemaillistarray, "Monthly"));
cron.schedule(yearlyunusualcronschedule, () => unusualactivityemail(yearlyemaillistarray, "Yearly"));


