const cron = require("node-cron");
const moment = require('moment')
const smtpTransporter = require("../helper/mail.js");
const { emailWithAttachment } = require("../helper/emailHelper.js");


const mysqldump = require("mysqldump");
const date = moment().format("YYYYMMDD");
const backupDatabasecronschedule = "* 00 * * *";

//cronjob for backing up of database by selecting the target user and file to send to
const backupDatabase = async () => {
    mysqldump({
        connection: {
            host: process.env.MYSQL_HOST,
            user: process.env.MYSQL_USER,
            password: process.env.MYSQL_PASSWORD,
            database: process.env.MYSQL_DATABASE,
            // host: process.env.SS_MYSQL_HOST,
            // user: process.env.SS_MYSQL_USER,
            // password: process.env.SS_MYSQL_PASSWORD,
            // database: process.env.SS_MYSQL_DATABASE,
        },
        dumpToFile: `cron_files/${date}_dump.sql`,
    });
    title = `${date} Back up of Database`;

    filename = `${date}_dump.sql`;
    recipient = process.env.CRON_BACKUP_DATABASE_RECIPIENT;

    smtpTransporter.sendMail(
        emailWithAttachment(title, filename, recipient),
        function (error, info) {
            if (error) {
                console.log(error);
            } else {
                console.log("Email sent: " + info.response);
            }
        }
    );
};

cron.schedule(backupDatabasecronschedule, () => backupDatabase());




