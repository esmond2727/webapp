var mysql = require('mysql');

const smtpTransporter = require("../helper/mail.js");
const { emailWithoutAttachment } = require("../helper/emailHelper.js");

const emailinfo = (country, username,ip,startdate) => {
    const immediateemailliststring = immediateemaillistarray.toString()
    title =  `${username} signed in from ${country}`
    recipient = immediateemailliststring
    text =  `We noticed some unusual activity on this account` 
    + '\n'
    + '\n'+`Username: ${username}` 
    + '\n'+`Country: ${country}`
    + '\n'+`IP Address: ${ip}`
    + '\n'+`Login Time: ${startdate}`


    smtpTransporter.sendMail(
        emailWithoutAttachment(title, recipient,text),
        function (error, info) {
            if (error) {
                console.log(error);
            } else {
                console.log("Email sent: " + info.response);
            }
        }
    );
}


const immediateemaillistarray = [];

        var con = mysql.createConnection({
          
                host: process.env.MYSQL_HOST,
                user: process.env.MYSQL_USER,
                password: process.env.MYSQL_PASSWORD,
                database: process.env.MYSQL_DATABASE,
                // host: process.env.SS_MYSQL_HOST,
                // user: process.env.SS_MYSQL_USER,
                // password: process.env.SS_MYSQL_PASSWORD,
                // database: process.env.SS_MYSQL_DATABASE,
             });
        con.connect(function(err) {
            if (err) throw err;
            console.log("Connected!");
            var sql = "SELECT email FROM users u join notification n on u.username = n.username join cronschedule c on c.id = n.emailfreq where cronfreq = 'Immediate';";
            console.log(sql)
            con.query(sql, function (err, result) {
                console.log(result)
                if (err) throw err;
                console.log("Table created");
                for (let i = 0; i < result.length; i++) {
                    immediateemaillistarray.push(result[i].email)
                }
            })
        })
        console.log(immediateemaillistarray)
        
        





module.exports = { emailinfo };


