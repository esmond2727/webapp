const cron = require('node-cron');
const fs = require('fs');
var mysql = require('mysql');


//cronschedule for updatelogouttime
const updatelogouttimecronschedule = "* * * * *";

// Remove the error.log file every twenty-first day of the month.
const updatelogouttime = async () => {
    console.log('---------------------');
    console.log('Running Cron Job');
    
    var con = mysql.createConnection({
      
            host: process.env.MYSQL_HOST,
            user: process.env.MYSQL_USER,
            password: process.env.MYSQL_PASSWORD,
            database: process.env.MYSQL_DATABASE,
            // host: process.env.SS_MYSQL_HOST,
            // user: process.env.SS_MYSQL_USER,
            // password: process.env.SS_MYSQL_PASSWORD,
            // database: process.env.SS_MYSQL_DATABASE,
        
         });
    con.connect(function(err) {
        if (err) throw err;
        console.log("Connected!");
        var sql = "update logs set  logouttimestamp = (logintimestamp+INTERVAL 8 MINUTE) where  (logouttimestamp is null) and  date(logintimestamp) = DATE_SUB(CURRENT_DATE(), INTERVAL 2 day);";
        console.log(sql)
        con.query(sql, function (err, result) {
            console.log(result)
            if (err) throw err;
            console.log("Table created");
        })
    })
  }

  cron.schedule(updatelogouttimecronschedule, () => updatelogouttime());

