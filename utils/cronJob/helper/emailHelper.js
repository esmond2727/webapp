const path = require("path");

//sending email with attachment template
const emailWithAttachment = (title, filename, recipient) => ({
    from: process.env.EMAIL,
    to : recipient,
    subject: title,
    text: `Please refer to the following attachment for the ${title}`,
    attachments: [
        {
            // stream as an attachment
            path: path.join(__dirname, `../../../cron_files/`, filename),
            filename: filename,
        },
    ],
});
//sending email without attachment template

const emailWithoutAttachment = (title, recipients, text) => ({
    from: process.env.EMAIL,
    to: recipients,
    subject: title,
    text: text,
});


        
        





module.exports = { emailWithAttachment, emailWithoutAttachment };
