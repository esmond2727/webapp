//dependencies
const cron = require("node-cron");
const moment = require("moment");
var mysql = require('mysql');

//constants
//Brute attack 1 : Login and logout within a minute to different id
const bruteattackcountarray1 = [];
//currently set to 60s = 1min
const bruteattackloginlogouttimelimit = 60;
//execute every min temporary set to 15 to prevent crashing
const bruteattackcronschedule1 = "* * * * *"

//Brute attack 2 : Brute Force attempt of login 20 times per min

const bruteattackcountarray2 = [];
//currently set to 20
const bruteattacktimelogincountpermin = 20;
//execute every min temporary set to 15 to prevent crashing
const bruteattackcronschedule2 = "15 * * * *"



        var con = mysql.createConnection({
          
                host: process.env.MYSQL_HOST,
                user: process.env.MYSQL_USER,
                password: process.env.MYSQL_PASSWORD,
                database: process.env.MYSQL_DATABASE,
            //     host: process.env.SS_MYSQL_HOST,
            // user: process.env.SS_MYSQL_USER,
            // password: process.env.SS_MYSQL_PASSWORD,
            // database: process.env.SS_MYSQL_DATABASE,
            
             });
        con.connect(function(err) {
            if (err) throw err;
            console.log("Connected!");
            
          

            //query example var sql9 = ` SELECT ip,count(*) as count from logs where  logintimestamp <  "2022-02-21 11:22:47" and  logintimestamp >= "2022-02-21 11:21:47" group by ip`;
            var sql9 = ` SELECT * from logs where  logintimestamp <  now() and  logintimestamp >=
             DATE_SUB(NOW(),INTERVAL 1 MINUTE)`;
            console.log(sql9)
            con.query(sql9, function (err, result9) {
                
                if (err) throw err;
                for (let i = 0; i < result9.length; i++) {
                    if(result9[i].count >bruteattacktimelogincountpermin)
                {
                    bruteattackcountarray1.push(result9[i].ip)
                }
                }
            })

             var sql10 = `
             SELECT id, sessionid, ip,mac, username, logintimestamp FROM (
                 SELECT *, ROW_NUMBER() OVER (PARTITION BY sessionid ORDER BY logintimestamp desc) AS ROWNUM 
                 FROM logs
             ) x WHERE ROWNUM = 1 order by id`;
            // console.log(sql8)
             con.query(sql10, function (err, result10) {
                 
                 if (err) throw err;
                //  for (let j = 1; j < result10.length; j++) {
                 for (let i = 0; i < result10.length-1; i++) {
                     let j = i + 1
                    // if (result10[j].logintimestamp !== undefined){
                   
                  
                    // }
                    // else{
                    for (let j = i + 1; j< result10.length-1; j++){
                    if(Math.abs(result10[j].logintimestamp-result10[i].logintimestamp)/1000 <= bruteattackloginlogouttimelimit &&
                     result10[j].ip === result10[i].ip)
                    {   
                    // console.log(i +"love"+ j)
                    // console.log(result10[j].logintimestamp)
                    // console.log(result10[i].logintimestamp)   
                    // console.log(result10[j].username)
                    // console.log(result10[i].username)   
                    // console.log(result10[i].ip)   
                    // console.log(result10[j].ip)
                    // console.log((Math.abs(result10[j].logintimestamp-result10[i].logintimestamp)/1000))
                    bruteattackcountarray2.push(result10[i].ip)

                     }
                    }
                //}
                }
                
             })
               // var sql6 = `SELECT ip,count( *) as count from logs where  logintimestamp <  "${now}" and  logintimestamp >= "${minago}" group by ip`;
            
        })
      
const automateblacklist2 = async () =>{

    const bruteattackcountarray2filter = bruteattackcountarray2.filter((data,index)=>{
        return bruteattackcountarray2.indexOf(data) === index;
      })
    console.log(bruteattackcountarray2filter)
    for (let i = 0; i < bruteattackcountarray2filter.length; i++) {
        var con = mysql.createConnection({
      
            // host: process.env.MYSQL_HOST,
            // user: process.env.MYSQL_USER,
            // password: process.env.MYSQL_PASSWORD,
            // database: process.env.MYSQL_DATABASE,
            host: process.env.SS_MYSQL_HOST,
            user: process.env.SS_MYSQL_USER,
            password: process.env.SS_MYSQL_PASSWORD,
            database: process.env.SS_MYSQL_DATABASE,
        
         });
    con.connect(function(err) {
        if (err) throw err;
        console.log("Connected!");
        var sql = `insert into blacklist(ipaddress) 
        SELECT * FROM (SELECT '${bruteattackcountarray2filter[i]}') AS tmp
        WHERE NOT EXISTS (
            SELECT ipaddress FROM webassess.blacklist WHERE ipaddress = '${bruteattackcountarray2filter[i]}'
        ) LIMIT 1`;
        con.query(sql, function (err, result) {
            console.log(result)
            if (err) throw err;
        })
    })
    }
}
const automateblacklist = async () =>{
    for (let i = 0; i < bruteattackcountarray1.length; i++) {
        var con = mysql.createConnection({
      
            // host: process.env.MYSQL_HOST,
            // user: process.env.MYSQL_USER,
            // password: process.env.MYSQL_PASSWORD,
            // database: process.env.MYSQL_DATABASE,
            host: process.env.SS_MYSQL_HOST,
            user: process.env.SS_MYSQL_USER,
            password: process.env.SS_MYSQL_PASSWORD,
            database: process.env.SS_MYSQL_DATABASE,
         });
    con.connect(function(err) {
        if (err) throw err;
        console.log("Connected!");
        var sql = `insert into blacklist(ipaddress) 
        SELECT * FROM (SELECT '${bruteattackcountarray1[i]}') AS tmp
        WHERE NOT EXISTS (
            SELECT ipaddress FROM webassess.blacklist WHERE ipaddress = '${bruteattackcountarray1[i]}'
        ) LIMIT 1`;
        con.query(sql, function (err, result) {
            console.log(result)
            if (err) throw err;
        })
    })
    }
  };






cron.schedule(bruteattackcronschedule1, () => automateblacklist());
cron.schedule(bruteattackcronschedule2, () => automateblacklist2());





