const cron = require("node-cron");
const fs = require("fs");
const path = require('path'); 


//Data Deleter Per Sem 1 (7 8 9 10 11 12) delete in 1st day of jan
const cronschedulesem1 ="0 0 1 1 *"

//Data Deleter Per Sem 2 (1 2 3 4 5 6) delete in 1st day of july
const cronschedulesem2 = "0 0 1 7 *" 

const directory = 'cron_files'; 


//delete files when new sem start


const dataDeleter = async () => {
    fs.readdir(directory, (err, files) => 
    { 
         console.log(path.join(directory))
        if (err) throw err; 
        for (const file of files) { 
         fs.unlink(path.join(directory, file), 
        
        err => { if (err) throw err; }); 
  } 
    });
    }


//Cron Schedule
cron.schedule(cronschedulesem1, () => dataDeleter());
cron.schedule(cronschedulesem2, () => dataDeleter());





